# Changelog

All notable changes to `laravel-complex-upsert` will be documented in this file.

## 5.2.1 - 2024-02-05
- clone template by default in validation
- optimize validation
- allow manual store relink
- improve nested data stat logging

## 5.2.0 - 2024-01-08
- Update whereInMulti method (add params NOT)

## 5.1.0 - 2023-05-03
- Improved comments
- Fixed prunning for pivot models
- Adding tests for the deletion feature
- Delete the correct ids in the list of to be deleted
- Adding failable feature to models in makeValid()
- Improved support for PHP variable references
- Improved recording of upsertedModels
- Correctly using the extended dispatcher finally callback
- Fix statistics for single process dispatchers

Backlog:
- fix the DIFF option for ReferenceCollection

## 5.0.0 - 2023-03-27
- Finally implemented the automatic model prunning feature
- Adding support for postgres
  - This package will now mainly developed for postgres instead of mysql. PR for mysql are still welcomed.
- Better logging for duplicated keys on incomming data
- Reduced default chunk size and adding an option for custom size.

## 4.3.0 - 2022-06-28
- Adding persistent reference collections
- Reference collections can now have multiple queries in order to fetch records in a cascading manner

## 4.2.0 - 2022-06-06
- Proper use of variable reference accross processor's functions
- Performance improvements

## 4.1.0 - 2022-05-10
- Better reference failure loggging
- Minor improvements

## 4.0.0 - 2022/05/04
Added:
- Defining WhereInMulti query as a Query Builder macro.
- Added Synchronous integration capability.
- Remote properties fetching improvements in upsertModels() step.
- MakeValid() now accepts a template instead of creating a new model.
- Other performance improvements.
  
Changes:
- Using a singleton instead of App::make() for services. Requires the use of cleanUp().
- Using json_encode() for the upsertKey generation. Types of attribute now matters.

## 3.0.0 - 2022/04/05

- Rework of reference resolution in order to improve performance
	- Using variable reference for upsert reference
	- Limit the use of Exception
	- Reduce objects creation (Relations, Exceptions, UpsertReference)
- Caching setup improvements
- Adding a Processor parameter which allow to bypass the caching system
- Minor performance improvements

## 2.3.0 - 2022/03/17

- Better Upsert type differenciation
- Separating Relation methodes from main Upsert trait.

## 2.2.0 - 2022/03/01

- Introducing a partial upsert management

## 2.1.2 - 2022/01/28

- Structure UpsertProcess statistics array

## 2.1.1 - 2022/01/28

- Include initial upsert models array in the chunk method.

## 2.1.0 - 2022/01/25

- Now present migrations as stubs and do not load them by default.
- Shared integration logging accross upsert engines.

## 2.0.0 - 2022/01/18

- Fixed incorrect tests indexes
- Introduced a process flow to manage upsert operations
- Upsert statistics are now logged in the database

## 1.4.0 - 2021/11/18

- Clear references at Job end (scoped services) to reduce memory usage
- References are now to be fetched before Upsert in a grouped manner
- It is now possible to specify at what point in the relation tree it is allowed to forget Models with failing references

## 1.3.0 - 2021/10/27

- Fix upsert references handling and synchronization

## 1.2.0 - 2021/10/13

- References are now handled on the referer
- Implementation was moved from extends to traits.

## 1.1.0 - 2021/09/21

- Converted Tests to Pest synthax.
- References can now also be upserted.
- Columns to be upserted can now be specified.

## 1.0.0 - 2021/09/09

- Adding tests
- Facilitating the use of this library by handling relations by passing the foreign relation name only.
- Pivots and References are now handled better.

## 0.2.x - 2021/08/27

- Reorganizing the package with [spatie/package-skeleton-laravel](https://github.com/spatie/package-skeleton-laravel)

## 0.1.x

- Implementation of basic capabilities.
- Relations are partially supported (requires manual configuration).
- Upsert request are grouped by type.
- Upserted models additional values like their unique ID are fetched upon saving and assigned to the model.
- References are only fetched once and stored statically.

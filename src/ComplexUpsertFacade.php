<?php

namespace Salaun\ComplexUpsert;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Salaun\ComplexUpsert\ComplexUpsert
 */
class ComplexUpsertFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'laravel-complex-upsert';
    }
}

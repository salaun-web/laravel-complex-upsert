<?php

namespace Salaun\ComplexUpsert;

use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Collection;
use Salaun\ComplexUpsert\Helpers\QueryBuilderHelper;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class ComplexUpsertServiceProvider extends PackageServiceProvider
{

	public function bootingPackage()
	{
		if (!QueryBuilder::hasMacro('whereInMulti')) {
			QueryBuilder::macro('whereInMulti', function (array $columns, array $values, string $defaultNull = "0", bool $not = false) {
				return QueryBuilderHelper::whereInMulti($this, $columns, $values, $defaultNull, $not);
			});
		}
		if (!QueryBuilder::hasMacro('getTableAlias')) {
			QueryBuilder::macro('getTableAlias', fn () => last(explode(' as ', $this instanceof JoinClause
				? $this->table
				: $this->from
			)));
		}
		EloquentBuilder::macro('upsertUnsafe', function (array $values, $uniqueBy, $update = null) {
			if (empty($values)) {
				return 0;
			}

			if (!is_array(reset($values))) {
				$values = [$values];
			}

			if (is_null($update)) {
				$update = array_keys(reset($values));
			}

			return QueryBuilderHelper::upsertUnsafe(
				$this->toBase(),
				$this->addTimestampsToUpsertValues($values),
				$uniqueBy,
				$this->addUpdatedAtToUpsertColumns($update),
			);
		});

		Collection::macro('mapByRef', function (callable $callback) {
			foreach ($this->items as $key => &$item) {
				$item = $callback($item, $key);
			}
			unset($item);
			return $this;
		});
	}

	public function configurePackage(Package $package): void
	{
		/*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
		$package
			->name('laravel-complex-upsert')
			->hasMigrations(
				'create_upsert_process_dispatchers_table',
				'create_upsert_processes_table',
				'create_upserted_models_table',
			)
			->hasConfigFile();

		$this->loadMigrationsFrom(__DIR__ . "/../database/migrations");
	}
}

<?php

namespace Salaun\ComplexUpsert\Interfaces;

interface UpsertReferenceable
{
    public function getId();
    public static function resolveReferences();
}

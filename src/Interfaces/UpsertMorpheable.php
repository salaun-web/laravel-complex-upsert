<?php

namespace Salaun\ComplexUpsert\Interfaces;

interface UpsertMorpheable
{
    public static function getMorphName(): string;
}

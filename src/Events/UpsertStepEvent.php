<?php

namespace Salaun\ComplexUpsert\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class UpsertStepEvent
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	public $name = null;
	public $nbElement = null;
	public $time = null;
	public $class = null;

	public $hasEnded = true;
	private $startEvent = null;


	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct(string $stepName, ?int $nbElement = null, ?string $class = null, bool $hasEnded = true, ?int $time = null)
	{
		$this->name = $stepName;
		$this->nbElement = $nbElement;
		$this->time = $time;
		$this->class = Str::snake(basename($class));
		$this->hasEnded = $hasEnded;
	}

	/**
	 * Calculate the time taken by the step in miliseconde.
	 *
	 * @return self::class
	 */
	public static function start(string $stepName, ?int $nbElement = null, ?string $class = null)
	{
		$event = new self($stepName, $nbElement, $class);
		$event->hasEnded = false;
		$event->startEvent = microtime(true);
		self::dispatch($event->name, $event->nbElement, $event->class, false);
		return $event;
	}

	/**
	 * Calculate the time taken by the step in miliseconde.
	 *
	 * @return void
	 */
	public function end()
	{
		$this->time = intval(
			(microtime(true) - $this->startEvent) * 1000
		);
		self::dispatch($this->name, $this->nbElement, $this->class, true, $this->time);
	}
}

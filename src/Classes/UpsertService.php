<?php

namespace Salaun\ComplexUpsert\Classes;

use stdClass;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Salaun\ComplexUpsert\Exceptions\UpsertConfigurationException;

class UpsertService
{
	private static ?UpsertService $service = null;

	// Makes sure that there are not 2 entries with the same unique key
	protected array $keysByTypes = [];
	// A cache to store the next generation (next children level)
	protected array $childrenByTypes = [];
	// To know what does not exist anymore (what is to be removed)
	//    we have to list existing records and exclude those we were given to upsert
	protected array $childrenByTypesToPrune = [];

	private function __construct()
	{
	}

	public static function get(): static
	{
		return static::$service === null
			? static::$service = new static()
			: static::$service;
	}

	/**
	 * Delete dangling record from the database
	 */
	public function pruneType(array $types = [], ?int $chunkSize = null)
	{
		$toPrune = collect(self::get()->childrenByTypesToPrune)
			->filter(fn (array $keys, string $class) => count($keys) > 0);
		if ($toPrune->isNotEmpty()) {
			// Log prunnable models if any
			Log::debug(
				'Found some records to prune',
				$toPrune->all()
			);

			if (is_null($chunkSize)) $chunkSize = config('complex-upsert.chunk_size');
			if (count($types) === 0) $types = array_keys($this->childrenByTypesToPrune);

			foreach ($types as $type) {
				if (array_key_exists($type, $this->childrenByTypesToPrune)) {
					$model = new $type();
					if ($model instanceof Pivot) {
						$upsertId = $model->getUpsertId();
						foreach (array_chunk(array_keys($this->childrenByTypesToPrune[$type]), $chunkSize) as $chunk) {
							foreach ($chunk as &$c) {
								$c = array_combine($upsertId, json_decode($c));
							}
							unset($c);
							$model->query()->whereInMulti($model->getUpsertId(), $chunk)->delete();
						}
					} else {
						foreach (array_chunk(array_keys($this->childrenByTypesToPrune[$type]), $chunkSize) as $chunk) {
							$model->query()->whereIn($model->getKeyName(), $chunk)->delete();
						}
					}
					unset($this->childrenByTypesToPrune[$type]);
				}
			}
		}
	}

	public static function cleanUp(bool $total = false)
	{
		static::$service = null;
	}

	/**
	 * Recursivelly upsert models and their relations grouping the elements by class.
	 *
	 * @param array $massModel
	 * @return void
	 */
	public static function massUpsert(array &$massModel, ?int $chunkSize = null): void
	{
		if (count($massModel) > 0) {
			if (is_null($chunkSize)) $chunkSize = config('complex-upsert.chunk_size');
			// Setup first run
			$mainClass = reset($massModel)::class;
			$childrenByTypes = &self::get()->childrenByTypes;
			$childrenByTypes[$mainClass] = &$massModel;
			// Run process as long as there are models to be processed
			while (count($childrenByTypes) > 0) {
				$keys = array_keys($childrenByTypes);
				// Models arrays only contains one Class of model.
				$class = reset($keys);
				$children = &$childrenByTypes[$class];
				unset($childrenByTypes[$class]);

				// We keep track of every unique key having being processed by class to find duplicates
				if (!array_key_exists($class, self::get()->keysByTypes)) {
					$childKeys = [];
					self::get()->keysByTypes[$class] = &$childKeys;
					unset($childKeys);
				}

				self::process(new $class, $children, self::get()->keysByTypes[$class], $chunkSize);
			}

			// Log duplicates if any
			$duplicateKeys = collect(self::get()->keysByTypes)->map(
				fn (array $keys, string $class) =>
				collect($keys)->filter(fn (int $count, string $key) => $count > 0)->all()
			)->filter(fn (array $keys, string $class) => count($keys) > 0)->all();

			if (count($duplicateKeys) > 0) {
				Log::error(
					'Found duplicated keys during upsert.',
					['keys' => json_encode(
						$duplicateKeys,
						JSON_PRETTY_PRINT
					)]
				);
			}
		}
	}

	/**
	 * Save the model and all of its relationships without throwing an Exception when parent isn't instanciated (unknown foreign key id).
	 *
	 * @param string $fkChildName	The laravel compliant foreign key name
	 * @param mixed $fkId			The id of the parent
	 * @return bool
	 */
	private static function process(Model $instance, array &$massModel, array &$processedKeys, int $chunkSize): void
	{
		$instanceKeyName = $instance->getKeyName();
		$instanceIsPivot = ($instance instanceof Pivot);
		$upsertQuery = $instance->query();
		$upsertId = $instance->getUpsertId();
		$upsertColumns = $instance->getUpsertColumns();
		$isSoftDelete = method_exists($instance, 'bootSoftDeletes');
		$returningAttributes = [$instanceKeyName];

		foreach (array_chunk($massModel, $chunkSize, true) as $chunkModel) {
			// Parse models to retrieve attributes
			$attributes = self::exportAttributes($chunkModel, $processedKeys, $isSoftDelete);

			// Insert or update if exist
			$upsertedIds = null;
			try {
				if ($upsertQuery->getConnection()->getDriverName() === "mysql" || $instanceIsPivot) {
					// There is no need to insure data validity as they have been retrieved with Model->getAttributes() and are thus already sanitized.
					$upsertQuery->clone()->upsertUnsafe($attributes, $upsertId, $upsertColumns);
				} else {
					$upsertedIds = $upsertQuery->clone()->upsertReturning($attributes, $upsertId, $upsertColumns, $returningAttributes)
						->map(fn (Model $returned) => $returned->getAttributes())->all();
				}
			} catch (Exception $e) {
				$instanceClass = $instance::class;
				$upsertIdList = collect($upsertId)->sort()->implode(',');
				$fieldLists = collect($attributes)				// All models as array
					->groupBy(
						fn (array $record) => collect($record)->keys()->sort()->implode(',')
					)											// Group by combination of fields name
					->map(fn (Collection $record) => $record->count())	// How many per combination
					->sortDesc()
					->map(fn (int $count, string $ids) => "{$ids} : {$count}")->implode("\n\t\t");
				throw new UpsertConfigurationException(
					message: <<<TXT
					Found failed records of class {$instanceClass}.
						Expected:
							{$upsertIdList}
						Received:
							{$fieldLists}
						Did you use makeValid()?
						Previous error was: {$e->getMessage()}
					TXT,
					class: $instanceClass,
					upsertId: $upsertId,
					failures: [],
					previous: $e,
				);
			}


			$toPrune = &static::get()->childrenByTypesToPrune;
			if ($instanceIsPivot) {
				// With pivots records the unique identifier is the upsertKey and not the primary key
				if (array_key_exists($instance::class, $toPrune)) {
					// We forget the unique identifiers we found in base
					$toPrune[$instance::class] =  array_diff_key($toPrune[$instance::class], $chunkModel);
				}
			} else {
				$nbModel = count($attributes);
				// In order to process the relations we need to bind the unique database ids to the Models we have.
				self::fetchRemoteProperties($chunkModel, $returningAttributes, $attributes, $processedKeys, $instance, $upsertedIds);
				$nbModelStored = count($chunkModel);
				if ($nbModel > $nbModelStored) {
					throw new Exception("Couldn't fetch all stored records of type " . $instance::class . " (Found {$nbModelStored} expected {$nbModel}).");
				}

				// Gather children by their types
				$relations = new Collection();
				foreach ($chunkModel as &$model) {
					// Remove this model (possibly a children) from the naughty list if it's on it
					$key = $model->getKey();
					if (array_key_exists($instance::class, $toPrune)) {
						if (array_key_exists($key, $toPrune[$instance::class])) unset($toPrune[$instance::class][$key]);
					}

					// Retrieve this models children
					foreach ($model->getRelations() as $relationKey => &$relationVal) {
						// Only reverse relations are related to the upsert process
						if (self::isReverseRelation($relationKey)) {
							// Prepare some values
							[$relationName, $reverseRelationName] = self::getReverseRelationNames($relationKey);
							// Preprare the prunning
							$childInstance = Collection::wrap($relationVal)->first();
							$classAndRelation = $childInstance::class . "::{$relationName}";
							if (!$relations->has($classAndRelation)) {
								if (!array_key_exists($childInstance::class, $toPrune)) $toPrune[$childInstance::class] = [];
								$eloquentRelation = $model->{$relationName}();
								$relations->put($classAndRelation, [
									...($childInstance instanceof Pivot)
										? $childInstance->getUpsertId()
										: [$childInstance->getKeyName()],
									...($eloquentRelation instanceof MorphTo)
										? [$eloquentRelation->getMorphType()]
										: [],
									$eloquentRelation->getForeignKeyName(),
								]);
							}
							// Prepare the upsert
							foreach ($relationVal as &$relatedChild) {
								// Retrieve the parent unique ids and assign it to the child using laravel's implementation
								$relatedChild->{$reverseRelationName}()->associate($model)->unsetRelation($reverseRelationName);
								self::get()->childrenByTypes[$relatedChild::class][] = &$relatedChild;
							}
							unset($relatedChild);
							// Rename reverse relation to common one
							$model->setRelation($relationName, $model->getRelation($relationKey))
								->unsetRelation($relationKey);
						}
					}
					unset($relationVal);
				}
				unset($model);

				if ($relations->isNotEmpty()) {
					static::listPrunnable($chunkModel, $relations, $toPrune);
				}
			}
		}
	}

	/**
	 *   List existing records to know what we will need to delete
	 */
	private static function listPrunnable($chunkModel, $relations, &$toPrune)
	{
		$load = [];
		$pluck = [];
		$relations->map(function ($keyName, $classAndRelation) use (&$load, &$pluck) {
			[$class, $relation] = explode('::', $classAndRelation);
			$load[] = "{$relation}:" . implode(',', array_unique($keyName));
			$pluck[$relation] = new $class();
		});

		// We have to break php references in order to preserve relations
		$eloquentCol = [];
		foreach ($chunkModel as $k => $c) $eloquentCol[$k] = (clone ($c))->setRelations([]);
		$eloquentCol = new EloquentCollection($eloquentCol);
		$eloquentCol->load($load);
		collect($pluck)->map(function ($obj, $relation) use ($eloquentCol, &$toPrune) {
			if ($obj instanceof Pivot) {
				foreach ($eloquentCol as &$parent) {
					$toPrune[$obj::class] += $parent->getRelation($relation)->map(
						fn ($rel) => $obj->getUpsertKeyAttribute($rel->getAttributes())
					)->flip()->all() ?? [];
					$parent->unsetRelation($relation);
				}
				unset($parent);
			} else {
				foreach ($eloquentCol as &$parent) {
					$toPrune[$obj::class] += Collection::wrap($parent->getRelation($relation))->map(fn ($rel) => $rel->getKey())->flip()->all() ?? [];
					$parent->unsetRelation($relation);
				}
				unset($parent);
			}
		});
	}

	private static function isReverseRelation(string $relationName): bool
	{
		return str_contains($relationName, '::');
	}

	private static function getReverseRelationNames(string $fullRelationName)
	{
		if (!self::isReverseRelation($fullRelationName)) throw new Exception('The given value was not a valid reverse relation.');
		return explode('::', $fullRelationName);
	}

	/**
	 * Extract the model's attributes and return them
	 * Indexes the models array with the model's unique upsertId
	 * In case of duplicated UpsertKey only the first Item will be kept. Sort the array properly in earlier steps in order to control this behaviour.
	 *
	 * @param array $models The raw array of models
	 * @return array $attributes An indexed array of attributes
	 */
	private static function exportAttributes(array &$models, array &$processedKeys, bool $isSoftDelete = false): array
	{
		$attributesHolder = [];
		$index = [];
		foreach ($models as $modelKey => &$modelVal) {
			$attributes = $modelVal->getAttributes();
			$key = $modelVal->getUpsertKeyAttribute($attributes);

			// The upsert key is by definition unique so we cannot allow to have multiple records with the same key
			// BUG: the duplicate check only applies on the chunk. Duplicates may occur accross chunk but will not be properly handled.
			if (array_key_exists($key, $processedKeys)) {
				$processedKeys[$key]++;
				$modelVal = null;
			} else {
				$processedKeys[$key] = 0;
				$attributesHolder[$key] = $attributes;
				$index[$key] = &$modelVal;
			}
		}
		unset($modelVal);
		$models = $index;

		// If the Model is soft-deleted we restore it by default
		if ($isSoftDelete) {
			foreach ($attributesHolder as $modelKey => &$modelVal) {
				// But if the value is already defined we let it as is.
				if (!array_key_exists('deleted_at', $modelVal)) $modelVal['deleted_at'] = null;
			}
			unset($modelVal);
		}

		return $attributesHolder;
	}

	/**
	 * For a given array of models, fetch their database properties and binds it to the local object.
	 * The Model array must be associative with the upsertKey as key.
	 *
	 * @param array $models
	 * @param array $propertiesToBind
	 * @return array $models The array of updated models with database's properties
	 */
	public static function fetchRemoteProperties(array $localModels, array $propertiesToBind = [], ?array $originalAttributes = null, array &$processedKeys = [], ?Model $modelSample = null, ?array $returning = null): array
	{
		$remoteModels = [];

		if (count($localModels) > 0) {
			if (is_null($returning)) {
				if ($modelSample === null) $modelSample = new (get_class(reset($localModels)));
				if ($originalAttributes === null) $originalAttributes = self::exportAttributes($localModels, $processedKeys);
				$upsertIds = $modelSample->getUpsertId();

				// Fields to update plus unique identifier fields to match with models
				$propertiesToBind = array_merge($propertiesToBind, $upsertIds);

				// Only returns updated models, as non existant models will not be found in the database
				$modelSample::select($propertiesToBind)->whereInMulti($upsertIds, $originalAttributes)
					// ->enforceOrderBy()
					->getQuery()
					// ->lazy()
					// ->cursor()
					->get()
					->each(
						function (stdClass $remoteModel) use ($upsertIds, &$localModels, &$remoteModels, &$originalAttributes) {
							// TODO: Retrieve properties using PDO::FETCH_ASSOC if Connection->setFetchMode() gets restored (cf. https://github.com/laravel/framework/issues/17557)
							$attributes = json_decode(json_encode($remoteModel), true);
							$key = static::generateUpsertKey($upsertIds, $attributes);
							if (array_key_exists($key, $localModels)) {
								// Assign ids to models
								$localModels[$key]->setRawAttributes(
									array_replace($originalAttributes[$key], $attributes)
								);
								$remoteModels[$key] = &$localModels[$key];
								unset($localModels[$key]);
								unset($originalAttributes[$key]);
							} else {
								Log::debug("Fetched a non-matching key {{$key}}.");
							}
						}
					);
			} else {
				$n = 0;
				foreach ($localModels as $key => &$model) {
					// Assign ids to models
					$localModels[$key]->setRawAttributes(
						array_replace($originalAttributes[$key], $returning[$n])
					);
					$remoteModels[$key] = &$localModels[$key];
					unset($localModels[$key]);
					unset($originalAttributes[$key]);
					$n++;
				}
				unset($model);
			}
		}

		if (count($localModels) > 0) {
			$logValues = [];
			foreach ($localModels as &$miss) {
				$logValues[] = $miss->getAttributes();
				$miss = null;
			}
			unset($miss);
			Log::debug('Some references could not be fetched bis: ' . json_encode($logValues, JSON_PRETTY_PRINT));
		}

		return $remoteModels;
	}

	/**
	 * Create a unique key for a given Model based on it's upsertIds
	 *
	 * Another option could be generating a hash for shorter keys and of same length
	 *
	 * @param [type] $model
	 * @return string The unique upsertId for the given model
	 */
	public static function generateUpsertKey(array $upsertId, array $values): string
	{
		$upsertIdValues = [];
		foreach ($upsertId as $id) {
			$upsertIdValues[] = $values[$id] ?? null;
		}
		return json_encode($upsertIdValues);
	}
}

<?php

namespace Salaun\ComplexUpsert\Classes;

use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Salaun\ComplexUpsert\Enums\ReferenceCollectionMode;
use Salaun\ComplexUpsert\Exceptions\UpsertReferenceException;
use Salaun\ComplexUpsert\Exceptions\UpsertReferenceFetchingException;

/**
 * The UpsertReferenceCollection is unique by it's query (reference class, tables used, filters and join config) and the reference Id used to retrieve the references.
 * It may be used by multiple classes.
 * Different classes may have different reference synchronization rules (columns) and thus sync data are grouped by syncKey.
 */
class UpsertReferenceCollection
{
	// The references that needs to be updated if any
	private array $classSyncColumns = [];

	// Every references to have been set
	private array $store = [];

	// the upsert keys of the models to be fetched
	private array $upsertRelated = [];
	// The database records of the references
	private array $upsertReferences = [];
	// The references that could not be found
	private array $upsertMissing = [];

	public function __construct(
		// Used by sync to upsert/sync data
		public string $class,
		public array $referenceId,
		public array $query,
		public ReferenceCollectionMode $persistent = ReferenceCollectionMode::NONE,
	) {
	}

	public function addClassSyncColumns(string $refererClass, string $relation, ?array $syncColumns)
	{
		$refererKey = self::getRefererKey($refererClass, $relation);
		if (!array_key_exists($refererKey, $this->classSyncColumns)) {
			$this->classSyncColumns[$refererKey] = [];
			$this->classSyncColumns[$refererKey]['columns'] = $syncColumns;
			$this->classSyncColumns[$refererKey]['upsertKeys'] = [];
		}
	}

	/**
	 * Define a reference that will have to be fetched in order to complete the upsert
	 *
	 * @param array $upsertId
	 * @param array $attributes
	 * @return void
	 */
	public function addUpsertReference(string $refererClass, string $relation, array $attributes): UpsertReference
	{
		// TODO filter sync and reference attributes only
		// Prepare references to be feched.
		$upsertKey = UpsertService::generateUpsertKey($this->referenceId, $attributes);

		// The multiWhereIn query arguments
		if (!array_key_exists($upsertKey, $this->store)) {
			$reference = UpsertReferenceService::getUpsertReferenceMain($relation, $refererClass);
			if (is_null($reference)) {
				UpsertReferenceService::setUpsertReferenceMain($relation, $refererClass, $this);
				$reference = UpsertReferenceService::getUpsertReferenceMain($relation, $refererClass);
			}
			$this->store[$upsertKey] = $reference->getInstance($attributes, $upsertKey);
			$this->upsertRelated[$upsertKey] = $upsertKey;
		}

		// Prepare reference to be synchronized.
		$refererKey = self::getRefererKey($refererClass, $relation);
		if (array_key_exists($refererKey, $this->classSyncColumns)) {
			if (!array_key_exists($upsertKey, $this->classSyncColumns[$refererKey])) {
				$this->classSyncColumns[$refererKey]['upsertKeys'][$upsertKey] = $upsertKey;
			}
		}
		return $this->store[$upsertKey];
	}

	/**
	 * Syncronize databases records.
	 *
	 * Trail for upgrade (upsert on joined tables)
	 * @link https://stackoverflow.com/questions/1382842/mysql-insert-joins
	 *
	 * @return void
	 */
	public function preFetch()
	{
		foreach ($this->classSyncColumns as $refererKey => $refererVal) {
			$syncAttributes = [];
			foreach ($refererVal['upsertKeys'] as $upsertKey) {
				if (!array_key_exists($upsertKey, $this->upsertReferences))
					$syncAttributes[] = $this->store[$upsertKey]->getAttributes();
			}
			if (count($syncAttributes) > 0) $this->class::upsert($syncAttributes, $this->referenceId, $refererVal['columns']);
			unset($this->classSyncColumns[$refererKey]);
		}
	}

	/**
	 * Retrieve databases records using the upsertRelated[] content.
	 *
	 * @return void
	 */
	public function fetch()
	{
		// We first check that there were references set to be fetched.
		if (count($this->upsertRelated) > 0) {
			// We exclude failed to fetch and already fetched records.
			// Especially important with persitent collections
			switch ($this->persistent) {
				case ReferenceCollectionMode::NONE:
					break;
					// BUG the DIFF option doesn't properly forgets items. the process tries to apply the relation on model with unset attributes
				case ReferenceCollectionMode::DIFF:
					$this->store = array_intersect_key($this->store, $this->upsertRelated);
					$this->upsertReferences = array_intersect_key($this->upsertReferences, $this->upsertRelated);
				case ReferenceCollectionMode::FULL:
					$this->upsertRelated = array_diff_key($this->upsertRelated,	$this->upsertReferences, $this->upsertMissing);
					break;
				default:
					throw new Exception("Reference collection mode not implemented ({$this->persistent}).");
			}

			// Download required references
			$upsertRelated = collect($this->upsertRelated);
			foreach ($this->query as $query) {
				if ($upsertRelated->isNotEmpty()) {
					$upsertRelated->chunk(200)->each(
						function (Collection $chunk) use ($query) {
							$query->clone()->whereInMulti(
								$this->referenceId,
								$chunk->map(fn (string $upsertKey) => $this->store[$upsertKey]->getAttributes())->all(),
							)->get()->each(function (Model $reference) use ($query) {
								if (!is_null($reference->getKey())) {
									// Index collection with the reference's unique upsertId.
									$key = UpsertService::generateUpsertKey($this->referenceId, $reference->getAttributes());
									if (array_key_exists($key, $this->store)) {
										$this->upsertReferences[$key] = $key;
										$this->store[$key]->setReference($reference);
										unset($this->upsertRelated[$key]);
									} else {
										Log::warning("Found a reference [{$query->getModel()->getTable()}] with no matching UpsertKey ({$key}). Make sure of the type of data you are setting as reference.");
									}
								}
							});
						}
					);
				}
			}

			if (count($this->upsertRelated) > 0) {
				// We log failed to fetch records
				foreach ($this->upsertRelated as $missingUpsertKey) {
					$this->upsertMissing[$missingUpsertKey] = [];
					$this->store[$missingUpsertKey]->isMissing = true;
					$this->store[$missingUpsertKey]->exception = new UpsertReferenceException("A reference couldn't be fetched ({$missingUpsertKey}).", $missingUpsertKey, $this->class);
				}
				$this->upsertRelated = [];
			}
		}
	}

	// Keys that, combined, are unique for any collection
	// TODO: generate a hash in order to flatten the service array of reference collections
	public static function getCollectionKey(Builder|array $query, array $referenceId): array
	{
		$sql = [];
		foreach ($query as $q) $sql[] = [$q->toSql(), $q->getBindings()];
		return ['referenceQuery' => json_encode($sql), 'referenceKey' => self::getReferenceKey($referenceId)];
	}

	public static function getRefererKey(string $class, string $relation): string
	{
		return "{$class}::{$relation}";
	}

	public static function getReferenceKey(?array $referenceId)
	{
		if ($referenceId === null) return null;
		sort($referenceId);
		return implode(',', $referenceId);
	}

	public function incrementMissing(string $upsertKey, string $class)
	{
		if (!array_key_exists($class, $this->upsertMissing[$upsertKey])) $this->upsertMissing[$upsertKey] += [$class => 0];
		$this->upsertMissing[$upsertKey][$class]++;
	}

	public function getStats($full = false)
	{
		return $full
			? [
				'upsertRelated' => $this->upsertRelated,
				'upsertReferences' => $this->upsertReferences,
				'upsertMissing' => $this->upsertMissing,
			]
			: [
				'upsertRelated' => count($this->upsertRelated),
				'upsertReferences' => count($this->upsertReferences),
				'upsertMissing' => count($this->upsertMissing),
			];
	}
}

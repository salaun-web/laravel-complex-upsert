<?php

namespace Salaun\ComplexUpsert\Classes;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Salaun\ComplexUpsert\Enums\ReferenceCollectionMode;

class UpsertReferenceService
{
	private static ?UpsertReferenceService $service = null;

	protected array $upsertReferenceMain = [];
	protected array $upsertReferenceCollections = [];

	private function __construct()
	{
	}

	public static function get()
	{
		return self::$service === null
			? static::$service = new static()
			: self::$service;
	}

	public static function cleanUp(bool $total = false)
	{
		if ($total) {
			static::$service = null;
		} else {
			// Collections
			$upsertReferenceCollections = &static::get()->upsertReferenceCollections;
			foreach ($upsertReferenceCollections as $sql => &$referer) {
				foreach ($referer as $referenceKey => &$collection) {
					if ($collection->persistent === ReferenceCollectionMode::NONE) $collection = null;
				}
				unset($collection);
				$referer = array_filter($referer);
				if (count($referer) === 0) $referer = null;
			}
			unset($referer);
			$upsertReferenceCollections = array_filter($upsertReferenceCollections);

			// Main references
			static::get()->upsertReferenceMain = [];
		}
	}

	public static function addUpsertReferenceCollection(
		// The class that made this collection
		string $refererClass,
		// The class that made this collection
		string $relation,
		// The class of which we try to get records of
		string $class,
		// The multiWhereIn query fields
		array $referenceId,
		// Allows to customize the reference fetching
		array $query,
		// Specify what values should be updated when sync if any. []: none (default), null: all, [x, y] x and y fields.
		?array $syncColumns = [],
		// Should this collection (and the data that have been fetched) be reused by the next job. Prevent memory leak.
		ReferenceCollectionMode $persistent = ReferenceCollectionMode::NONE,
	): array {
		$collections = &self::get()->upsertReferenceCollections;
		$collectionKey = UpsertReferenceCollection::getCollectionKey($query, $referenceId);
		if (!array_key_exists($collectionKey['referenceQuery'], $collections))
			$collections[$collectionKey['referenceQuery']] = [];

		if (!array_key_exists($collectionKey['referenceKey'], $collections[$collectionKey['referenceQuery']])) {
			$collections[$collectionKey['referenceQuery']][$collectionKey['referenceKey']] =
				new UpsertReferenceCollection($class, $referenceId, $query, $persistent);
		}

		if ($syncColumns !== []) {
			self::getUpsertReferenceCollection(...$collectionKey)->addClassSyncColumns($refererClass, $relation, $syncColumns);
		}

		return $collectionKey;
	}

	public static function setUpsertReferenceMain(string $relation, string $class, UpsertReferenceCollection $collection)
	{
		static::get()->upsertReferenceMain[$class][$relation] = new UpsertReference(
			$collection,
			$class,
			$relation,
		);
	}

	public static function getUpsertReferenceMain(string $relation, string $class): ?UpsertReference
	{
		return static::get()->upsertReferenceMain[$class][$relation] ?? null;
	}

	public static function getUpsertReferenceCollection(string $referenceQuery, string $referenceKey): UpsertReferenceCollection
	{
		return self::get()->upsertReferenceCollections[$referenceQuery][$referenceKey];
	}

	public static function resolveUpsertRefences(array &$instances): void
	{
		$collections = &self::get()->upsertReferenceCollections;
		foreach ($collections as $sqlStr => $referer) {
			foreach ($referer as $referenceKey => $collection) {
				$collection->preFetch();
			}
		}

		foreach ($collections as $sqlStr => $referer) {
			foreach ($referer as $referenceKey => $collection) {
				// try {
				$collection->fetch();
				// } catch (UpsertReferenceFetchingException $e) {
				// 	Log::error("Failed to fetch a reference.", ['referenceKey' => $referenceKey, 'extra' => json_encode($e->getExtra(), JSON_PRETTY_PRINT), 'exception' => $e]);
				// }
			}
		}

		foreach ($instances as &$instance) {
			UpsertReferenceService::resolveReferences($instance);
			if ($instance instanceof Exception) {
				throw $instance;
			}
		}
		unset($instance);
		$instances = array_filter($instances);
	}

	/**
	 * Models need to resolve the reference with a database value before the upsert query.
	 *
	 * @link https://laravel.com/docs/8.x/eloquent-relationships#polymorphic-relationships
	 * @link https://laracasts.com/discuss/channels/eloquent/eloquent-sync-associate?reply=28904
	 *
	 * @return static
	 */
	public static function resolveReferences(Model &$instance): void
	{
		$canFail = $instance->canFail();
		// We check if the model have references (if the trait for the references is being used).
		if (isset($instance->upsertReferenceCache)) {
			foreach ($instance->upsertReferenceCache as $relationName => $reference) {
				$reference->resolve($instance);
				if ($instance instanceof Exception) {
					if ($canFail) $instance = null;
					return;
				}

				// Unset the reference as it has already been processed
				unset($instance->upsertReferenceCache[$relationName]);
			}
		}
		$relations = $instance->getRelations();
		if (count($relations) > 0) {
			foreach ($relations as $relationName => &$relation) {
				if ($relation instanceof Collection) $relation = $relation->filter()->all();
				if (is_array($relation)) {
					foreach ($relation as &$related) {
						if (method_exists($related, 'canFail')) {
							static::resolveReferences($related);
							if ($related instanceof Exception) {
								$instance = $canFail ? null : $related;
								return;
							}
						}
					}
					unset($related);
					$relation = array_filter($relation);
				} else {
					if (method_exists($relation, 'canFail')) {
						static::resolveReferences($relation);
						if ($relation instanceof Exception) {
							$instance = $canFail ? null : $relation;
							return;
						}
					}
				}
			}
			unset($relation);
			$relations = array_filter($relations, fn ($relation) => ($relation !== null) && ($relation !== []));
			$instance->setRelations($relations);
		}
	}

	public static function getStats()
	{
		$stats = [];
		$collections = static::get()->upsertReferenceCollections;
		foreach ($collections as $sqlStr => $referer) {
			$stats[$sqlStr] = [];
			foreach ($referer as $referenceKey => $collection) {
				$stats[$sqlStr][$referenceKey] = $collection->getStats();
			}
		}
		return $stats;
	}

	public static function getMissings()
	{
		$stats = [];
		$collections = static::get()->upsertReferenceCollections;
		foreach ($collections as $sqlStr => $referer) {
			$stats[$sqlStr] = [];
			foreach ($referer as $referenceKey => $collection) {
				$missings = $collection->getStats(true)['upsertMissing'];
				if (count($missings) > 0)
					$stats[$sqlStr][$referenceKey] = $missings;
			}
			if (count($stats[$sqlStr]) === 0) unset($stats[$sqlStr]);
		}
		return $stats;
	}
}

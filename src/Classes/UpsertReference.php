<?php

namespace Salaun\ComplexUpsert\Classes;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use JsonSerializable;
use Salaun\ComplexUpsert\Exceptions\UpsertReferenceException;

class UpsertReference implements JsonSerializable
{
	// The "answer". The database model we are looking for.
	public Model $reference;
	// The "instance" key. Relevant columns values implode
	public string $upsertKey;
	// The "question". The values for which we are looking the database reference
	public array $attributes;

	public bool $isMorphic = false;
	public bool $isMissing = false;
	public bool $canFail = false;

	public UpsertReferenceException $exception;

	public string $foreignKey;
	public string $foreignClass;

	public string|int $key;

	public function __construct(
		// The main repository where all the "questions" are asked in order to get "answers".
		public UpsertReferenceCollection &$collection,
		string $parentClass,
		string $relationName,
	) {
		$parent = new $parentClass();
		$relation = $parent->{$relationName}();
		switch ($relation::class) {
			case MorphTo::class:
				$this->foreignClass = $relation->getRelated()::class;
			case BelongsTo::class:
				$this->foreignKey = $relation->getForeignKeyName();
				break;
			default:
				throw new Exception('Only support one to one relationships for references.');
		}
	}

	public function getInstance(array $attributes, string $upsertKey): UpsertReference
	{
		$instance = clone $this;
		$instance->upsertKey = $upsertKey;
		$instance->attributes = $attributes;

		return $instance;
	}

	public function getCollection()
	{
		return $this->collection;
	}

	public function resolve(Model &$parent): void
	{

		if ($this->isMissing) {
			$this->getCollection()->incrementMissing($this->upsertKey, $parent::class);
			$parent = $this->canFail ? null : $this->exception;
			return;
		}
		$attributes = $parent->getAttributes();
		if ($this->isMorphic) {
			$attributes[$this->foreignClass] = $this->reference::class;
		}
		$attributes[$this->foreignKey] = $this->key;

		$parent->setRawAttributes($attributes);
	}

	public function getAttributes()
	{
		return $this->attributes;
	}

	public function setReference(Model &$reference)
	{
		$this->reference = &$reference;
		$this->key = $this->reference->getKey();
		return $this;
	}

	public function jsonSerialize(): mixed
	{
		return $this->upsertKey;
	}
}

<?php

namespace Salaun\ComplexUpsert\Jobs;

use Illuminate\Bus\Batch;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\PendingBatch;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Log;
use Salaun\ComplexUpsert\Models\UpsertProcess;
use Salaun\ComplexUpsert\Models\UpsertProcessDispatcher;

abstract class UpsertProcessorDispatcher implements ShouldQueue
{
	use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	protected int $processDispatcherId;
	protected bool $full;

	protected UpsertProcessDispatcher $processDispatcher;

	private array $jobs = [];
	private Collection $data;
	protected $subject;

	/**
	 * @link https://github.com/laravel/framework/issues/34296 Jobs of a batch must have the same queue
	 */
	public static function dispatchBus(UpsertProcessDispatcher &$processDispatcher, string $queue = 'default', ?callable $finally = null, bool $full = false): PendingBatch
	{
		static::checkForConcurrentUpsert($processDispatcher);
		if (!App::runningInConsole()) $processDispatcher->user()->associate(Auth::user());
		if ($finally === null) $finally = static::finallyCallback();
		$processDispatcher->save();

		$dispatcher = new static();
		$dispatcher->full = $full;
		$dispatcher->processDispatcherId = $processDispatcher->getKey();

		$bus = Bus::batch([$dispatcher])->finally($finally);

		$processDispatcher->batch_id = $bus->onQueue($queue)->dispatch()->id;
		$processDispatcher->save();

		return $bus;
	}

	protected static function finallyCallback()
	{
		return function (Batch $batch) {
			$dispatcher = UpsertProcessDispatcher::where('batch_id', $batch->id)->with('processes')->sole();
			if (!$batch->cancelled() && $batch->failedJobs === 0) {
				$dispatcher = static::mergeStats($dispatcher);
			}
			$dispatcher->batch_id = null;
			$dispatcher->save();
		};
	}

	public static function mergeStats(UpsertProcessDispatcher $dispatcher): UpsertProcessDispatcher
	{
		$stats = new Collection();
		$processCount = $dispatcher->processes->count();
		if ($processCount !== 0) {
			if ($processCount === 1) {
				$dispatcher->statistics = $dispatcher->processes->first()->statistics;
			} else {
				$dispatcher->processes->each(function ($process) use (&$stats) {
					$stats = $stats->mergeRecursive(collect($process->statistics)->only(['time', 'store']));
				});
				$dispatcher->statistics = $stats->map(
					fn ($stat) => collect($stat)->map(
						fn ($type) => (is_array($type) && count($type) > 0)
							? collect($type)->sum()
							: 0
					)
				);
			}
		}
		return $dispatcher;
	}

	/**
	 * Throw an exception if there is already a process for the same subject
	 */
	protected static function checkForConcurrentUpsert(UpsertProcessDispatcher $processDispatcher): void
	{
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle(): void
	{
		$batch = $this->batch();
		if ($batch === null || $batch->cancelled()) return;

		if ($this->prepareDispatcher($this->processDispatcherId, $this->subject)) {
			$this->data = $this->getData($this->subject, $this->processDispatcher);
			if (count($this->data) > 0) {
				$this->batch()->add(
					static::getJobs($this->data, $this->processDispatcher, $this->full)
				);
				$this->processDispatcher->batch_id = $this->batchId;
				$this->processDispatcher->push();
			} else {
				Log::notice("Integration cancelled: no data to process.");
			}
		} else {
			Log::error('Could not find the processDispatcher record');
		}
		$this->processDispatcher->save();
	}

	/**
	 * Fetch the matching processDispather record and check if this dispatcher should continue.
	 */
	protected function prepareDispatcher($processDispatcherId, $subject): bool
	{
		$this->processDispatcher = UpsertProcessDispatcher::find($processDispatcherId);

		return $this->processDispatcher !== null;
	}

	abstract public static function getData($subject): Collection;

	public static function getJobs(Collection $datas, UpsertProcessDispatcher $processDispatcher, bool $full): array
	{
		$jobs = [];
		foreach ($datas as $data) {
			if ($data instanceof Collection) $data = $data->all();
			$jobs[] = static::prepareProcessorJob($data, $processDispatcher, $full);
		}
		return $jobs;
	}

	protected static function prepareProcessorJob(array $data = [], UpsertProcessDispatcher $processDispatcher, bool $full)
	{
		$process = new UpsertProcess();
		$process->processDispatcher()->associate($processDispatcher);
		$processor = new UpsertProcessor(process: $process, full: $full);
		return $processor;
	}
}

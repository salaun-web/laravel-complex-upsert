<?php

namespace Salaun\ComplexUpsert\Jobs;

use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Salaun\ComplexUpsert\Classes\UpsertReferenceService;
use Salaun\ComplexUpsert\Classes\UpsertService;
use Salaun\ComplexUpsert\Models\UpsertedModel;
use Salaun\ComplexUpsert\Models\UpsertProcess;
use Salaun\ComplexUpsert\Models\UpsertProcessDispatcher;
use Throwable;

abstract class UpsertProcessor implements ShouldQueue
{
	use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected UpsertProcess $process;

	// The element container that is passed from a step to another
	public Collection|array $primaryStore;
	public Collection|array $secondaryStore;
	// Root models being updated
	protected Collection $processed;
	// List of the previous processing hashes regarding related models
	protected Collection $upserted;
	// List of the current processing hashes regarding related models
	protected Collection $upsertedHashes;

	// List the ids of elements having passed the confirmUpsert step
	protected Collection $upserting;
	// List the ids of elements eliminated by the confirmUpsert step
	protected Collection $upsertingConfirmed;

	public bool $reLinkStores = true;

	// Each processor has it's database record to store statistics and relation to hashes
	protected int $processId;

	// Each step alter the $store
	// If an element fails to passe a step or is found to not have to be updated (confirmUpsert), it is removed from the $store.
	protected static array $steps = [
		"getData",
		"instanciateData",
		"confirmUpsert",
		"transformData",
		"resolveReferences",
		"upsertModels",
		"prunningModels",
	];

	protected string $step;

	// If elements are grouped on a parent it is sometimes usefull to operate on a single group so that we can work with same sized chunks
	// Those steps will be given $subStore, a flatten array of elements
	// It works with references so **be carefull to set elements to null then filter to eliminate them** when needed.
	protected static array $multiPassSteps = [];

	public function __construct(
		UpsertProcess $process,
		?UpsertProcessDispatcher $processDispatcher = null,
		?array $ids = null,
		protected bool $full = false,
	) {
		if ($processDispatcher !== null) {
			$process->processDispatcher()->associate($processDispatcher);
		}
		if ($ids !== null) {
			$process->ids = $ids;
		}
		$process->save();
		$this->processId = $process->getKey();
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$batch = $this->batch();
		if ($batch !== null && $batch->cancelled()) return;

		$this->process = $this->getProcess();

		$this->primaryStore = new Collection();
		$this->secondaryStore = new Collection();
		$this->processed = new Collection();
		$this->upserted = new Collection();
		$this->upsertedHashes = new Collection();
		$this->upserting = new Collection();
		$this->upsertingConfirmed = new Collection();

		$totalTime = microtime(true);

		$stats = $this->process->statistics;

		$this->beforeUspert($this->processed, $this->upserted, $this->upsertedHashes);

		foreach (static::$steps as $step) {
			// Sometimes it is better to not have the elements grouped by parents
			if (in_array($step, static::$multiPassSteps)) {
				$this->secondaryStore = $this->secondaryStore->filter();

				$this->runStep($step, $this->secondaryStore, $stats);

				// After a "multiPass" step the subStore is filtered (as a main store would be), but we need to clean the main store's children
				$this->primaryStore = $this->primaryStore->mapByRef(function (Collection $storeChildren) {
					// For every parent we remove the children set to null
					$storeChildren = $storeChildren->filter();
					// If the parent lose all it's children we remove the parent, else we return it's children
					return $storeChildren->isEmpty() ? null : $storeChildren;
				})->filter();
			} else {
				$this->runStep($step, $this->primaryStore, $stats);
			}

			if ($step === 'instanciateData' || $this->reLinkStores) {
				$this->linkStores($this->primaryStore->all(), count(static::$multiPassSteps) > 0);
				$this->reLinkStores = false;
			}
		}

		$this->afterUpsert($this->primaryStore);

		$stats['reference'] = UpsertReferenceService::getStats();
		$stats['time']['total'] = round(microtime(true) - $totalTime, 3);
		$stats['memory']['total'] = memory_get_usage();
		$this->process->statistics = $stats;
		$this->process->save();

		$this->serviceDown(false);
	}

	/**
	 * Handle a job failure.
	 *
	 * @param  \Throwable  $exception
	 * @return void
	 */
	public function failed(Throwable $exception)
	{
		$this->serviceDown(true);
	}

	protected function serviceDown(bool $purge)
	{
		// Hard reset
		UpsertService::cleanUp();
		UpsertReferenceService::cleanUp($purge);
	}

	protected function runStep(string $step, Collection &$store, array &$stats): void
	{
		$this->step = $step;
		if (version_compare(PHP_VERSION, '8.2', '>=')) {
			memory_reset_peak_usage();
		}
		$time = microtime(true);
		$stats['store'][$step] = $this->{$step}($store);
		$stats['time'][$step] = round(microtime(true) - $time, 3);
		$stats['memory'][$step] = memory_get_peak_usage();
	}

	public function getProcess(): UpsertProcess
	{
		return UpsertProcess::with('processDispatcher.model')->find($this->processId);
	}

	protected function linkStores(array $primaryStore, bool $isDeep = false)
	{
		// As long as the stores are well maintained during steps a single setup is enough
		// an element needs to be referenced at least twice in order not to be garbage collected
		$primary = [];
		$secondary = [];

		if ($isDeep) {
			// So we make a subStore with all the children by refrence
			foreach ($primaryStore as $primaryKey => $primaryVal) {
				$primary[$primaryKey] = [];
				$subStore = $primaryVal->all();
				// By using a reference, the object is not cloned so it doesn't duplicate it in RAM
				// This way when an object is changed in one of the array it is also changed in the other
				// But you have to be carefull to preserve the reference and not clone the object in each step
				foreach ($subStore as $key => &$item) {
					// We assigne the reference to the main store (grouped array)
					$primary[$primaryKey][$key] = &$item;
					// And to the substore (flat array)
					$secondary[] = &$item;
				}
				unset($item);
				$primary[$primaryKey] = collect($primary[$primaryKey]);
			}
			unset($subStore);
		} else {
			foreach ($primaryStore as $key => &$item) {
				$primary[$key] = &$item;
				$secondary[$key] = &$item;
			}
			unset($item);
		}

		$this->primaryStore = collect($primary);
		$this->secondaryStore = collect($secondary);
	}

	protected static function newUpserted(array $attributes = []): UpsertedModel
	{
		return new UpsertedModel($attributes);
	}

	public function filterStore(Collection &$store): int
	{
		if (in_array($this->step, static::$multiPassSteps) || empty(static::$multiPassSteps)) {
			$store = $store->filter();
			return $store->count();
		}
		$sum = 0;
		foreach ($store->all() as $key => &$val) {
			$val = $val->filter();
			$sum += $val->count();
		}
		return $sum;
	}

	/**
	 * Retrieve previous upsert data
	 * Query note: should only select data required by this process and cache informations
	 */
	public function beforeUspert(Collection &$processed, Collection &$upserted, Collection &$upsertedHashes)
	{
		// Get the related upserted models
	}

	/**
	 * Fetch the raw data from the source
	 * Query note: should not execute any database queries
	 */
	abstract public function getData(Collection &$store): int;

	/**
	 * Make objects from the raw data
	 * Query note: should not execute any database queries
	 */
	abstract public function instanciateData(Collection &$store): int;

	/**
	 * Remove data that doesn't needs to be processed as it has not changed since last upsert.
	 * Query note: should not execute any database queries
	 */
	public function confirmUpsert(Collection &$store): int
	{
		$store->mapByRef(function ($item, $key) {
			$keeper = true;
			$hash = hash('murmur3f', is_string($item) ? $item : json_encode($item));
			if ($this->upsertedHashes->has($hash)) {
				$upsertedKey = $this->upsertedHashes->get($hash);
				$upserting = $this->upserted->get($upsertedKey);
				$keeper = $this->full;
				if (!$this->full) $this->upsertingConfirmed->add($key);
			} else {
				$upserting = static::newUpserted(['data_hash' => $hash]);
			}
			$upserting->process()->associate($this->process);
			$this->upserting->put($key, $upserting);

			if (!$keeper) $item = null;
			return $item;
		});

		unset($this->upsertedHashes);
		return $this->filterStore($store);
	}

	/**
	 * Transform and restructure the raw objects to match it's final form.
	 * The type of values, data structure and references can be configured here.
	 * Query note: should not execute any database queries
	 */
	abstract public function transformData(Collection &$store): int;

	/**
	 * Fetch from the database the records that need to be linked to the models to be upserted
	 * Query note: should only execute select and/or update queries on the references
	 */
	public function resolveReferences(Collection &$store): int
	{
		$store = $store->all();
		UpsertReferenceService::resolveUpsertRefences($store);
		$missing = UpsertReferenceService::getMissings();
		if (count($missing) > 0) Log::warning('Some references could not be resolved: ' . json_encode($missing, JSON_PRETTY_PRINT));

		$store = collect($store);
		return $this->filterStore($store);
	}

	/**
	 * Proceed to update the database with the processed and validated data
	 * Query note: should only execute update for the data and it's relations
	 */
	public function upsertModels(Collection &$store): int
	{
		$store = $store->all();
		UpsertService::massUpsert($store);
		$store = collect($store);

		return $this->filterStore($store);
	}

	/**
	 * Proceed to update the database with the processed and validated data
	 * Query note: should only execute delete queries
	 */
	public function prunningModels(Collection &$store): int
	{
		UpsertService::get()->pruneType();

		return $store->count();
	}

	/**
	 * Save the "caching" informations to be used by the next upsert.
	 * This is only done for the elements that have passed every steps of this upsert.
	 * Query note: should only execute update queries on the cache informations
	 */
	public function afterUpsert(Collection &$store)
	{
		// Prepare
		$upsertings = new Collection();
		$duplicates = [];

		// Indexing on upsertable_id prevents issue in the case where the source has multiple elements with the same upsertKey
		// When it's not a full upsert, one of the duplicates will match the hash and be eliminated (cached)
		// But the others will be passed to later steps. One of those will be kept for upsetModels and saved in base.
		// So one upserted and one in store will end up here. We keep the store.

		// Adding cached UpsertedModels to show they are still being send by the data provider
		// and update their hashes
		$this->upsertingConfirmed->each(
			function ($key) use (&$upsertings) {
				$u = $this->upserting->pull($key)->replicate();
				$upsertings->put(
					$u->upsertable_id,
					// remove unecessary attributes
					$u->getAttributes(),
				);
			}
		);

		// Adding newly upserted
		$store->each(
			function ($model, $key) use (&$upsertings, &$duplicates) {
				$u = $this->upserting->pull($key)->replicate()
					// attach the upserted model
					->upsertable()->associate($model);
				if ($upsertings->has($u->upsertable_id)) {
					$duplicates[] = $model->getUpsertKeyAttribute();
				}
				$upsertings->put(
					$u->upsertable_id,
					// remove unecessary attributes
					$u->getAttributes(),
				);
			}
		);

		if (count($duplicates) > 0) {
			Log::error("Found duplicate upserted", ['upsertKeys' => $duplicates]);
		}

		UpsertedModel::upsert(
			$upsertings->all(),
			static::newUpserted()->getUpsertId(),
		);
	}
}

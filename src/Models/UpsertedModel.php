<?php

namespace Salaun\ComplexUpsert\Models;

use Illuminate\Database\Eloquent\Model;
use Salaun\ComplexUpsert\Traits\HasUpserts;

class UpsertedModel extends Model
{
	use HasUpserts;

	public const MORPH_UPSERTABLE = 'upsertable';

	protected $attributes = [
		"upsert_process_id" => null,
		'upsertable_id' => null,
		'upsertable_type' => null,
	];

	protected $table = 'upserted_models';

	protected static array $upsertId = [
		'upsert_process_id',
		'upsertable_id',
		'upsertable_type',
	];

	protected $fillable = [
		'data_hash',
	];

	public function process()
	{
		return $this->belongsTo(UpsertProcess::class, 'upsert_process_id');
	}

	public function upsertable()
	{
		return $this->morphTo();
	}
}

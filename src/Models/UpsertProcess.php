<?php

namespace Salaun\ComplexUpsert\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Salaun\ComplexUpsert\Traits\HasUpserts;

class UpsertProcess extends Model
{
	use HasUpserts;

	protected static array $upsertId = [
		'id',
	];

	protected $table = 'upsert_processes';

	protected $fillable = [
		'ids',
		'statistics',
	];

	protected $attributes = [
		'ids' => '[]',
		'statistics' => <<< JSON
		{"store" : [],	"time" : [], "reference" : [], "memory" : []}
		JSON,
	];

	protected $casts = [
		'ids' => 'array',
		'statistics' => 'array',
	];

	protected static string $upsertName;

	public function __construct(array $attributes = [])
	{
		parent::__construct($attributes);
		$this->setAttribute('upsert_name', static::getUpsertName());
	}

	public static function getUpsertName()
	{
		return isset(static::$upsertName)
			? static::$upsertName
			: static::$upsertName = static::class;
	}

	/**
	 * The "booted" method of the model.
	 *
	 * @return void
	 */
	protected static function booted()
	{
		static::addGlobalScope('differenciate_process', function (Builder $builder) {
			$builder->when(
				self::class !== static::getUpsertName(),
				fn (Builder $query)
				=> $query->where('upsert_name', '=', static::getUpsertName())
			);
		});
	}

	public function processDispatcher()
	{
		return $this->belongsTo(UpsertProcessDispatcher::class, 'upsert_process_dispatcher_id');
	}

	public function upsertedModels()
	{
		return $this->hasMany(UpsertedModel::class);
	}
}

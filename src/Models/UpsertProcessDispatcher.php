<?php

namespace Salaun\ComplexUpsert\Models;

use Illuminate\Bus\Batch;
use Illuminate\Foundation\Auth\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Bus;

class UpsertProcessDispatcher extends Model
{
	protected $table = 'upsert_process_dispatchers';

	protected $fillable = [
		'batch_id',
		'ids',
		'statistics',
		// 'cancelled',
	];

	protected $attributes = [
		'ids' => '[]',
		'statistics' => '[]',
		// 'cancelled' => 0,
	];

	/**
	 * The attributes that should be cast.
	 *
	 * @var array
	 */
	protected $casts = [
		'ids' => 'array',
		'statistics' => 'array',
		// 'cancelled' => 'boolean',
	];

	public function __construct(array $attributes = [], protected ?Model $model = null)
	{
		if ($this->model !== null) $this->processDispatchable()->associate($model);
		parent::__construct($attributes);
	}

	public function getBatchAttribute(): ?Batch
	{
		if (!$this->relationLoaded('batch')) {
			$batch = null;
			if ($this->batch_id !== null) {
				$batch = Bus::findBatch($this->batch_id);
			}
			$this->setRelation('batch', $batch);
		}
		return $this->getRelation('batch');
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function model()
	{
		return $this->morphTo();
	}

	public function processes()
	{
		return $this->hasMany(UpsertProcess::class, 'upsert_process_dispatcher_id');
	}

	public function upserted()
	{
		return $this->hasManyThrough(related: UpsertedModel::class, through: UpsertProcess::class, firstKey: 'upsert_process_dispatcher_id', secondKey: 'upsert_process_id');
	}
}

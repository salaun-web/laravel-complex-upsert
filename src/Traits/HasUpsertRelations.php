<?php

namespace Salaun\ComplexUpsert\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * This is to be used by all models involved
 */
trait HasUpsertRelations
{
	/**
	 * A helper allowing the addition of a relation to an existing Collection
	 *
	 * @param string $relationName
	 * @param Model $relation
	 * @return static
	 */
	public function addRelation(string $relationName, Model $relation): static
	{
		if (!$this->relationLoaded($relationName)) $this->setRelation($relationName, []);
		$this->setRelation($relationName, [...$this->getRelation($relationName), $relation]);

		return $this;
	}

	/**
	 * Adds a reverse relation without erasing previous ones
	 *
	 * @param Model $relation
	 * @param string|null $relationName
	 * @return static
	 */
	public function addReverseRelation(Model $relation, string $relationName = null, ?string $reverseRelationName = null): static
	{
		if ($relationName === null) $relationName = Str::of(class_basename($relation::class))->plural()->camel()->__toString();
		if ($reverseRelationName === null) $reverseRelationName = Str::camel(class_basename($this::class));
		$this->addRelation("{$relationName}::{$reverseRelationName}", $relation);

		return $this;
	}

	/**
	 * Defines a reverse relation
	 *
	 * @param Collection $relations
	 * @param string|null $relationName
	 * @return static
	 */
	public function setReverseRelation(Collection $relations, ?string $relationName = null, ?string $reverseRelationName = null): static
	{
		if ($relationName === null) $relationName = Str::of(class_basename($relations->first()::class))->plural()->camel()->__toString();
		if ($reverseRelationName === null) $reverseRelationName = Str::camel(class_basename($this::class));
		$this->setRelation("{$relationName}::{$reverseRelationName}", $relations);

		return $this;
	}
}

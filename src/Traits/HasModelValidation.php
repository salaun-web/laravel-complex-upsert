<?php

namespace Salaun\ComplexUpsert\Traits;

use Illuminate\Contracts\Validation\Validator as ValidationValidator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Salaun\ComplexUpsert\Helpers\UpsertValidator;

trait HasModelValidation
{
	protected static ?array $rules = null;
	protected static ?UpsertValidator $validator = null;

	/**
	 * This function should return a singleton of the rules validations.
	 * Example:
	 * - placeholder: protected static ?array $rules;
	 * - getter: protected static function getRules(){
	 * 	return (static::$rules === null) ? static::$rules : static::$rules = ['name' => ['required', 'size:255' ], 'password' => Password::min(8)];
	 */
	abstract protected static function getRules(): array;

	protected static function getValidator(array $data): UpsertValidator
	{
		if (is_null(static::$validator)) {
			static::$validator = new UpsertValidator(
				(Validator::make([], []))->getTranslator(),
				[],
				static::getRules()
			);
		}
		$validator = clone static::$validator;
		return $validator->resetData($data);
	}


	/**
	 * Create a new validated upsert model instance.
	 *
	 * @param  array  $attributes
	 * @return static
	 */
	public static function makeValid(
		array	$attributes = [],
		array	$relations = [],
		array	$references = [],
		bool	$validate = true,
		?Model	$template = null,
	): ?static {
		if ($template !== null) {
			$template = clone ($template);
			$attributes = array_replace($template->getAttributes(), $attributes);
		}

		$validated = true;
		$upsertModel = null;
		if ($validate) {
			$validator = static::getValidator(array_merge($attributes, $relations, $references));
			if ($validator->fails()) {
				if (!(new static())->canFail())	$validator->throwException();
				$validated = false;
			}
		}
		if ($validated) {
			$upsertModel = ($template === null) ? new static($attributes) : $template->setRawAttributes($attributes);
			collect($relations)->filter()->each(function (Collection $relations, $relationName) use (&$upsertModel) {
				$upsertModel->setReverseRelation($relations, ...explode(':', $relationName));
			});
			collect($references)->filter()->each(fn($referenceAttributes, $referenceName) => $upsertModel->setUpsertReference($referenceName, $referenceAttributes));
		}
		return $upsertModel;
	}
}

<?php

namespace Salaun\ComplexUpsert\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Salaun\ComplexUpsert\Classes\UpsertReference;
use Salaun\ComplexUpsert\Classes\UpsertReferenceService;
use Salaun\ComplexUpsert\Enums\ReferenceCollectionMode;

trait HasUpsertReferenceCollections
{
	public array $upsertReferenceCache = [];

	/**
	 * Initialize the collections
	 *
	 * @return static
	 */
	abstract public function registerUpsertReferenceCollections(): static;

	/**
	 * Makes sure all required fields are set, prepare the reference model for fetching & register the reference on this instance.
	 *
	 * @param string $relationName
	 * @param array $attributes
	 * @return static
	 */
	public function addUpsertReferenceCollection(string $relation, array $referenceId, Builder|array|null $query = null, ?array $syncColumns = [], ReferenceCollectionMode $persistent = ReferenceCollectionMode::NONE): static
	{
		// The instanciation of this collection's class
		$instance = $this->{$relation}()->getRelated();
		// The class to which this collection is linked.
		$class = $instance::class;
		// Query used to retrieve the database records
		$query = ($query === null)
			? [$instance->query()]
			: $query = Arr::wrap($query);
		// Register the reference collection
		// $this->upsertReferenceCache[$relation]['canFail'] = $canFail;
		$collectionKey = UpsertReferenceService::addUpsertReferenceCollection(static::class, $relation, $class, $referenceId, $query, $syncColumns, $persistent);
		UpsertReferenceService::setUpsertReferenceMain(
			$relation,
			static::class,
			UpsertReferenceService::getUpsertReferenceCollection(...$collectionKey),
		);
		return $this;
	}

	public function getUpsertReference(string $relation): UpsertReference
	{
		$referenceMain = UpsertReferenceService::getUpsertReferenceMain($relation, static::class);
		// Using relations prevent duplicate collections and allows to get default values
		if ($referenceMain  === null) {
			$this->registerUpsertReferenceCollections();
			$referenceMain = UpsertReferenceService::getUpsertReferenceMain($relation, static::class);
		}

		return $referenceMain;
	}

	/**
	 * Add a set of values as reference to be processed later
	 *
	 * @param string $relationName
	 * @param array $attributes
	 * @return static
	 */
	public function setUpsertReference(string $relation, array $referenceAttributes): static
	{
		$this->upsertReferenceCache[$relation] =  $this
			->getUpsertReference($relation)
			->getCollection()
			->addUpsertReference(static::class, $relation, $referenceAttributes);

		return $this;
	}
}

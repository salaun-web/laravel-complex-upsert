<?php

namespace Salaun\ComplexUpsert\Traits;

use Exception;
use Salaun\ComplexUpsert\Classes\UpsertService;

/**
 * This is to be used by all models involved
 */
trait HasUpserts
{
	use HasOrderedJsonAttributes;
	use HasUpsertRelations;

	// protected static array $upsertId = [];
	// protected static ?array $upsertColumns = null;

	public function canFail(): bool
	{
		return isset(static::$canFail)
			? static::$canFail
			: false;
	}

	public function getRawAttribute($key)
	{
		$attribute = null;
		if (array_key_exists($key, $this->attributes)) $attribute = $this->attributes[$key];
		return $attribute;
	}

	public function setRawAttribute($key, $attribute)
	{
		$this->attributes[$key] = $attribute;
	}

	/**
	 * Return the Model upsertId.
	 *
	 * @return array
	 */
	public function getUpsertId(): array
	{
		if (!isset(static::$upsertId)) throw new Exception('Missing upsert ids!');
		return static::$upsertId;
	}

	/**
	 * Return the UpsertKey for this instance of UpsertModel.
	 *
	 * @return void
	 */
	public function getUpsertKeyAttribute(?array $attributes = null)
	{
		return UpsertService::generateUpsertKey(
			$this->getUpsertId(),
			($attributes === null) ? $this->getAttributes() : $attributes
		);
	}

	/**
	 * Define what database fields are to be updated with local data.
	 *
	 * Values correspond to the third upsert() parameter:
	 * - "Null" will insert only.
	 * - Empty array will update all.
	 * - Array with fields will only update those.
	 *
	 * @return void
	 *
	 */
	public function getUpsertColumns(): ?array
	{
		return isset(static::$upsertColumns)
			? static::$upsertColumns
			: null;
	}
}

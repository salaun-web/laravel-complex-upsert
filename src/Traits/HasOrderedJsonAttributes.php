<?php

namespace Salaun\ComplexUpsert\Traits;

trait HasOrderedJsonAttributes
{
	/**
	 * Convert the model instance to an array.
	 *
	 * @return array
	 */
	public function toArray()
	{
		// Sort the attributes in order to produce a deterministic Hash of the model and it's attributes
		$attributes = $this->attributesToArray();
		ksort($attributes);
		$relations = $this->relationsToArray();
		ksort($relations);
		$upsertReferenceCache = (isset($this->upsertReferenceCache)) ? $this->upsertReferenceCache : [];
		ksort($upsertReferenceCache);

		$array = array_merge($attributes, $relations, ['upsertReferenceCache' => $upsertReferenceCache]);
		return $array;
	}
}

<?php

namespace Salaun\ComplexUpsert\Exceptions;

use Exception;

class UpsertReferenceException extends Exception
{
	public function __construct(
		string $message,
		private string $upsertKey,
		private string $class,
	) {
		parent::__construct($message);
	}

	public function getUpsertKey()
	{
		return $this->upsertKey;
	}

	public function getClass()
	{
		return $this->class;
	}
}

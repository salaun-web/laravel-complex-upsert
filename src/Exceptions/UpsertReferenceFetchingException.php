<?php

namespace Salaun\ComplexUpsert\Exceptions;

use Exception;

class UpsertReferenceFetchingException extends Exception
{

	public function __construct(
		string $message,
		private array $extra,
	) {
		parent::__construct($message);
	}

	public function getExtra(): array
	{
		return $this->extra;
	}
}

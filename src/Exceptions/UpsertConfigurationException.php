<?php

namespace Salaun\ComplexUpsert\Exceptions;

use Exception;
use Throwable;

class UpsertConfigurationException extends Exception
{
	public function __construct(
		string $message,
		public string $class,
		public array $upsertId,
		public array $failures,
		?Throwable $previous = null,
	) {
		parent::__construct(
			message: $message,
			previous: $previous,
		);
	}
}

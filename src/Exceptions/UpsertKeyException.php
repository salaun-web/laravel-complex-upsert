<?php

namespace Salaun\ComplexUpsert\Exceptions;

use Exception;

class UpsertKeyException extends Exception
{
	public function __construct(
		string $message,
		public string $id,
		public array $upsertId,
		public array $values,
		public string $class,
	) {
		parent::__construct($message);
	}

	public function getId()
	{
		return $this->id;
	}

	public function getClass()
	{
		return $this->class;
	}

	public function setClass()
	{
		return $this->class;
	}
}

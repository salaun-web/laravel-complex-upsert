<?php

namespace Salaun\ComplexUpsert\Helpers;

use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator;

class UpsertValidator extends Validator
{
	/**
	 * Parse the data array, converting dots and asterisks.
	 *
	 * @param  array  $data
	 * @return static
	 */
	public function resetData(array $data): static
	{
		$this->data = $this->parseData($data);
		return $this;
	}

	/**
	 * Parse the data array, converting dots and asterisks.
	 *
	 * @param  array  $data
	 * @return array
	 */
	public function throwException()
	{

		throw new \Exception(
			'The given data was invalid.'
				. json_encode($this->errors(), JSON_PRETTY_PRINT)
				. ' '
				. json_encode($this->getData(), JSON_PRETTY_PRINT)
				. '.'
		);

		throw_if(true, $this->exception, $this);
	}
}

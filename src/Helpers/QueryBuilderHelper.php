<?php

namespace Salaun\ComplexUpsert\Helpers;

use Illuminate\Support\Arr;
use Doctrine\DBAL\Query\QueryBuilder;

class QueryBuilderHelper
{
	/**
	 * Elloquent doesn't support (yet) a multi parameter where in query, so this is a raw implementation
	 * Exemple with 2 parameters: WHERE (param0, param1) IN ((val0, val1), (val0', val1'))
	 *
	 * @link https://stackoverflow.com/a/34182218/11499112
	 *
	 * @param [type] $query Any Elloquent query to be extended
	 * @param array $columns The parameters of the whereIn query
	 * @param array $values All the available values ordered by the parameter's names
	 * @param string $defaultNull Where In request with multiple fields does not allow Null as a correct value (it is ignore). NVL()/IFNULL() allows to replace them with a placeholder string (cf. StackOverflow link).
	 * @return $query The input query to which the whereInMulti has been added
	 */
	public static function whereInMulti($query, array $columns, array $values, string $defaultNull = '-1', bool $not = false)
	{
		$columnCount = count($columns);

		// If there is only one parameter we can use the native whereIn() function
		if ($columnCount > 1) {
			$nullableColumn = [];
			$bindings = [];
			// Prepare values
			foreach ($values as &$value) {
				// only select the unique identifier fields
				$samples = [];
				foreach ($columns as $column) {
					$binding = $value[$column] ?? null;
					if ($binding === null) {
						// Search for fields that may contain Null value in order to "escape" them witn NVL() as they are not supported.
						if (!array_key_exists($column, $nullableColumn)) $nullableColumn[$column] = $column;
						$bindings[] = $defaultNull;
					} else {
						$bindings[] = $binding;
					}
					$samples[] = '?';
				}
				$value = '(' . implode(', ', $samples) . ')';
			}
			unset($value);

			foreach ($columns as &$column) {
				$wrappedColumn = $query->grammar->wrap($column);
				$column = (array_key_exists($column, $nullableColumn)) ? "COALESCE({$wrappedColumn}, ?)" : "{$wrappedColumn}";
			}
			unset($column);
			$operator = $not ? 'NOT IN' : 'IN';
			$query->whereRaw(
				'(' . implode(', ', $columns) . ') ' . $operator . ' (' . implode(', ', $values) . ')',
				array_merge(array_fill(0, count($nullableColumn), $defaultNull), $bindings),
			);
		} else {
			$column = reset($columns);
			$query->whereIn(column: $column, values: array_column($values, $column), not: $not);
		}

		return $query;
	}

	/**
	 * Insert new records or update the existing ones.
	 * This is an adaptation of QueryBuilder::upsert() but without the data sanitization for performances concernes
	 *
	 * @param  array  $values
	 * @param  array|string  $uniqueBy
	 * @param  array|null  $update
	 * @return int
	 */
	public static function upsertUnsafe($query, array $values, $uniqueBy, $update = null)
	{
		if (empty($values)) {
			return 0;
		} elseif ($update === []) {
			return (int) $query->insert($values);
		}

		if (!is_array(reset($values))) {
			$values = [$values];
		} else {
			foreach ($values as $key => $value) {
				ksort($value);

				$values[$key] = $value;
			}
		}

		if (is_null($update)) {
			$update = array_keys(reset($values));
		}

		$query->applyBeforeQueryCallbacks();

		return $query->connection->affectingStatement(
			$query->grammar->compileUpsert($query, $values, (array) $uniqueBy, $update),
			Arr::flatten($values, 1)
		);
	}

	/**
	 * Insert new records or update the existing ones.
	 * This is an adaptation of QueryBuilder::upsert() but without the data sanitization for performances concernes
	 *
	 * @param  array  $values
	 * @param  array|string  $uniqueBy
	 * @param  array|null  $update
	 * @return int
	 */
	public static function upsertUnsafeReturning($query, array $values, $uniqueBy, $update = null)
	{
		if (empty($values)) {
			return 0;
		} elseif ($update === []) {
			return (int) $query->insert($values);
		}

		if (!is_array(reset($values))) {
			$values = [$values];
		} else {
			foreach ($values as $key => $value) {
				ksort($value);

				$values[$key] = $value;
			}
		}

		if (is_null($update)) {
			$update = array_keys(reset($values));
		}

		$query->applyBeforeQueryCallbacks();

		return $query->connection->affectingStatement(
			$query->grammar->compileUpsert($query, $values, (array) $uniqueBy, $update),
			Arr::flatten($values, 1)
		);
	}
}

<?php

namespace Salaun\ComplexUpsert\Enums;

enum ReferenceCollectionMode: string
{
	case NONE = 'none';
	case FULL = 'full';
	case DIFF = 'differential';
}

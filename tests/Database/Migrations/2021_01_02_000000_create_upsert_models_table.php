<?php

use Illuminate\Database\Migrations\Migration;
use Tpetry\PostgresqlEnhanced\Schema\Blueprint;
use Tpetry\PostgresqlEnhanced\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('upsert_models', function (Blueprint $table) {
			$table->id();
			$table->foreignId('upsert_reference_id')->nullable()->constrained()->cascade();
			$table->char('lang', 2);
			$table->string('name');
			$table->string('slug');
			$table->string('value');
			$table->string('optional')->nullable();

			$table->softDeletes();
			$table->timestamps();

			match (DB::connection($this->connection)->getDriverName()) {
				'pgsql' => $table->uniqueIndex(['lang', 'slug', 'upsert_reference_id'])->nullsNotDistinct(),
				'mysql' => $table->unique(['lang', 'slug', 'upsert_reference_id']),
			};
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('upsert_models');
	}
};

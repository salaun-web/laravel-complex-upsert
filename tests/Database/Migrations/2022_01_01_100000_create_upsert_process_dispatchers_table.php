<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('upsert_process_dispatchers', function (Blueprint $table) {
			$table->id();

			$table->foreignId('user_id')->nullable()->constrained();
			$table->nullableMorphs('model');
			$table->string('batch_id')->nullable(); // Not constrained because Jobs doesn't have to be handled by the database.

			$table->json('ids');
			$table->json('statistics');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('upsert_process_dispatchers');
	}
};

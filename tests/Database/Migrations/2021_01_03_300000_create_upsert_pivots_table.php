<?php

use Illuminate\Database\Migrations\Migration;
use Tpetry\PostgresqlEnhanced\Schema\Blueprint;
use Tpetry\PostgresqlEnhanced\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('upsert_pivots', function (Blueprint $table) {
			$table->foreignId('upsert_model_id')->constrained();
			$table->foreignId('upsert_pivoted_id')->constrained('upsert_pivoted');
			$table->boolean('active');

			$table->timestamps();

			match (DB::connection($this->connection)->getDriverName()) {
				'pgsql' => $table->uniqueIndex(['upsert_model_id', 'upsert_pivoted_id'])->nullsNotDistinct(),
				'mysql' => $table->unique(['upsert_model_id', 'upsert_pivoted_id']),
			};
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('upsert_pivots');
	}
};

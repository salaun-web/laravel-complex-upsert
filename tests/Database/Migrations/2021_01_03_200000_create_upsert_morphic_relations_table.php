<?php

use Illuminate\Database\Migrations\Migration;
use Tpetry\PostgresqlEnhanced\Schema\Blueprint;
use Tpetry\PostgresqlEnhanced\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('upsert_morphic_relations', function (Blueprint $table) {
			$table->id();
			$table->morphs('upsert_morphic_relationable', 'upsert_morphic_relations_relationable_type_relationable_id_index');
			$table->string('name');
			$table->string('description');

			$table->timestamps();

			match (DB::connection($this->connection)->getDriverName()) {
				'pgsql' => $table->uniqueIndex(['upsert_morphic_relationable_type', 'name'], 'upsert_morphic_relationable_name_unique')->nullsNotDistinct(),
				'mysql' => $table->unique(['upsert_morphic_relationable_type', 'name'], 'upsert_morphic_relationable_name_unique'),
			};
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('upsert_morphic_relations');
	}
};

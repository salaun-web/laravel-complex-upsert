<?php

use Illuminate\Database\Migrations\Migration;
use Tpetry\PostgresqlEnhanced\Schema\Blueprint;
use Tpetry\PostgresqlEnhanced\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('upsert_references', function (Blueprint $table) {
			$table->id();
			$table->string('name');
			$table->string('description');

			$table->softDeletes();
			$table->timestamps();

			match (DB::connection($this->connection)->getDriverName()) {
				'pgsql' => $table->uniqueIndex(['name'])->nullsNotDistinct(),
				'mysql' => $table->unique(['name']),
			};
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('upsert_references');
	}
};

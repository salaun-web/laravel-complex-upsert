<?php

use Illuminate\Database\Migrations\Migration;
use Tpetry\PostgresqlEnhanced\Schema\Blueprint;
use Tpetry\PostgresqlEnhanced\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('upserted_models', function (Blueprint $table) {
			$table->id();

			$table->foreignId('upsert_process_id')->constrained()
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->morphs('upsertable');

			$table->char('data_hash', 32)->nullable();
			$table->timestamps();


			match (DB::connection($this->connection)->getDriverName()) {
				'pgsql' => $table->uniqueIndex(
					['upsert_process_id', 'upsertable_id', 'upsertable_type'],
					'upserted_models_upsert_key'
				)->nullsNotDistinct(),
				'mysql' => $table->unique(
					['upsert_process_id', 'upsertable_id', 'upsertable_type'],
					'upserted_models_upsert_key'
				),
			};
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('upserted_models');
	}
};

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('upsert_processes', function (Blueprint $table) {
			$table->id();

			$table->foreignId('upsert_process_dispatcher_id')->nullable()->constrained();

			$table->string('upsert_name');
			$table->json('ids');
			$table->json('statistics');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('upsert_processes');
	}
};

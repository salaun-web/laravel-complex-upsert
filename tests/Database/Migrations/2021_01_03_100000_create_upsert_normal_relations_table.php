<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Tpetry\PostgresqlEnhanced\Schema\Blueprint;
use Tpetry\PostgresqlEnhanced\Support\Facades\Schema;

return new class extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('upsert_normal_relations', function (Blueprint $table) {
			$table->id();
			$table->foreignId('upsert_model_id')->constrained()->cascade();
			$table->string('name');
			$table->string('description');

			$table->timestamps();

			match (DB::connection($this->connection)->getDriverName()) {
				'pgsql' => $table->uniqueIndex(['name'])->nullsNotDistinct(),
				'mysql' => $table->unique(['name']),
			};
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('upsert_normal_relations');
	}
};

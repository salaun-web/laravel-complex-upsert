<?php

namespace Salaun\ComplexUpsert\Tests\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertModel;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertMorphicRelation;

class UpsertMorphicRelationFactory extends Factory
{
	/**
	 * The name of the factory's corresponding model.
	 *
	 * @var string
	 */
	protected $model = UpsertMorphicRelation::class;

	/**
	 * Define the model's default state.
	 *
	 * @return array
	 */
	public function definition()
	{
		return [
			'upsert_morphic_relationable_id' => UpsertModel::factory(),
			'upsert_morphic_relationable_type' => fn ($attributes) => UpsertModel::find($attributes['upsert_morphic_relationable_id'])->type,
			'name' => $this->faker->unique()->name(),
			'description' => $this->faker->sentence(),
		];
	}
}

<?php

namespace Salaun\ComplexUpsert\Tests\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertNormalRelation;

class UpsertNormalRelationFactory extends Factory
{
	/**
	 * The name of the factory's corresponding model.
	 *
	 * @var string
	 */
	protected $model = UpsertNormalRelation::class;

	/**
	 * Define the model's default state.
	 *
	 * @return array
	 */
	public function definition()
	{
		return [
			'name' => $this->faker->unique()->name(),
			'description' => $this->faker->sentence(),
		];
	}
}

<?php

namespace Salaun\ComplexUpsert\Tests\Database\Factories;

use App\Models\Model;
use Illuminate\Database\Eloquent\Factories\Factory;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertPivoted;

class UpsertPivotedFactory extends Factory
{
	/**
	 * The name of the factory's corresponding model.
	 *
	 * @var string
	 */
	protected $model = UpsertPivoted::class;

	/**
	 * Define the model's default state.
	 *
	 * @return array
	 */
	public function definition()
	{
		return [
			'name' => $this->faker->unique()->name(),
			'description' => $this->faker->sentence(),
		];
	}
}

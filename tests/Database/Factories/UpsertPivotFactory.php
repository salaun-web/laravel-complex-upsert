<?php

namespace Salaun\ComplexUpsert\Tests\Database\Factories;

use App\Models\Model;
use Illuminate\Database\Eloquent\Factories\Factory;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertModel;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertPivot;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertPivoted;

class UpsertPivotFactory extends Factory
{
	/**
	 * The name of the factory's corresponding model.
	 *
	 * @var string
	 */
	protected $model = UpsertPivot::class;

	/**
	 * Define the model's default state.
	 *
	 * @return array
	 */
	public function definition()
	{
		return [
			'upsert_model_id' => UpsertModel::factory(),
			'upsert_pivoted_id' => UpsertPivoted::factory(),
			'active' => $this->faker->boolean(),
		];
	}
}

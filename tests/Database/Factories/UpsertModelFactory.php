<?php

namespace Salaun\ComplexUpsert\Tests\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertModel;

class UpsertModelFactory extends Factory
{
	/**
	 * The name of the factory's corresponding model.
	 *
	 * @var string
	 */
	protected $model = UpsertModel::class;

	/**
	 * Define the model's default state.
	 *
	 * @return array
	 */
	public function definition()
	{
		return [
			'lang'	=> $this->faker->countryCode(),
			'name'	=> $this->faker->text(15),
			'value'	=> $this->faker->randomNumber(5),
		];
	}
}

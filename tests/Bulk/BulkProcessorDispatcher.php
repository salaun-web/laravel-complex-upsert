<?php

namespace Salaun\ComplexUpsert\Tests\Bulk;

use Illuminate\Support\Collection;
use Salaun\ComplexUpsert\Jobs\UpsertProcessor;
use Salaun\ComplexUpsert\Jobs\UpsertProcessorDispatcher;
use Salaun\ComplexUpsert\Models\UpsertProcessDispatcher;

class BulkProcessorDispatcher extends UpsertProcessorDispatcher
{
	public static function getData($subject, ?UpsertProcessDispatcher $processDispatcher = null): Collection
	{
		return collect($processDispatcher->ids)->map(fn ($item) => [$item]);
	}

	public static function prepareProcessorJob(array $ids = [], UpsertProcessDispatcher $processDispatcher, bool $full): UpsertProcessor
	{
		$process = new BulkProcess();
		return new BulkProcessor($process, $processDispatcher, $ids);
	}
}

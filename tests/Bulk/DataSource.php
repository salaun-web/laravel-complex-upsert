<?php

namespace Salaun\ComplexUpsert\Tests\Bulk;

use Salaun\ComplexUpsert\Tests\TestClasses\UpsertModel;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertNormalRelation;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertReference;

class DataSource
{
	public static function getIds()
	{
		return ['A', 'B'];
	}

	public static function getData($id = null)
	{
		$refA = ['name' => 'a ref A', 'description' => 'the ref A description'];
		$refB = ['name' => 'a ref B', 'description' => 'the ref B description'];

		UpsertReference::upsert([$refA, $refB], ['name']);

		$seed = [
			'A' => (new UpsertModel([
				"lang" => "AA",
				"name" => "a name A",
				"value" => "a value A",
			]))->setReverseRelation(collect([
				new UpsertNormalRelation([
					"name" => "a normal relation A0",
					"description" => "a normal relation's description A0",
				]),
				new UpsertNormalRelation([
					"name" => "a normal relation A1",
					"description" => "a normal relation's description A1",
				])
			]))->setUpsertReference('upsertReference', $refA),
			'B' => (new UpsertModel([
				"lang" => "BB",
				"name" => "a name B",
				"value" => "a value B",
			]))->setReverseRelation(collect([
				new UpsertNormalRelation([
					"name" => "a normal relation B0",
					"description" => "a normal relation's description B0",
				]),
				new UpsertNormalRelation([
					"name" => "a normal relation B1",
					"description" => "a normal relation's description B1",
				])
			]))->setUpsertReference('upsertReference', $refB),
		];

		return match ($id) {
			'A'		=> $seed['A'],
			'B'		=> $seed['B'],
			default	=> $seed,
		};
	}

	public static function fetchData($id = null)
	{
		return json_encode(static::getData($id));
	}

	public static function parseData($id = null)
	{
		return static::getData($id);
	}
}

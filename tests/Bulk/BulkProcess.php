<?php

namespace Salaun\ComplexUpsert\Tests\Bulk;

use Salaun\ComplexUpsert\Models\UpsertProcess;

class BulkProcess extends UpsertProcess
{
	public function processDispatcher()
	{
		return $this->belongsTo(BulkProcessDispatcher::class, 'upsert_process_dispatcher_id');
	}
}

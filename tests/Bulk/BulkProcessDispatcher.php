<?php

namespace Salaun\ComplexUpsert\Tests\Bulk;

use Salaun\ComplexUpsert\Models\UpsertProcessDispatcher;

class BulkProcessDispatcher extends UpsertProcessDispatcher
{
	public function model()
	{
		return $this->morphTo();
	}
}

<?php

namespace Salaun\ComplexUpsert\Tests\Bulk;

use Illuminate\Support\Collection;
use Salaun\ComplexUpsert\Jobs\UpsertProcessor;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertModel;

class BulkProcessor extends UpsertProcessor
{
	public function beforeUspert(Collection &$processed, Collection &$upserted, Collection &$upsertedHashes)
	{
		$upserted = UpsertModel::with('latestUpsertedModel')->get()->keyBy('id');
		$upsertedHashes = $this->upserted->filter(
			fn (UpsertModel $upserted) => $upserted->latestUpsertedModel?->data_hash !== null
		)->mapWithKeys(
			fn (UpsertModel $upsertModel) => [$upsertModel->latestUpsertedModel->data_hash => $upsertModel->id]
		);
	}

	public function getData(Collection &$store): int
	{

		$ids = count($this->process->ids) > 0 ? $this->process->ids : null;


		if ($ids === null) {
			foreach (DataSource::parseData() as $id => $data) {
				$store->put($id, $data);
			}
		} else {
			foreach ($ids as $id) {
				$store->put($id, DataSource::fetchData($id));
			}
		}

		return $this->filterStore($store);
	}

	public function instanciateData(Collection &$store): int
	{
		$store->transform(fn ($item) => json_decode($item));
		return static::filterStore($store);
	}

	public function transformData(Collection &$store): int
	{
		$ids = count($store) > 0 ? $store->keys() : null;

		$store = new Collection();

		if ($ids === null) {
			foreach (DataSource::parseData() as $key => $data) {
				$store->put($key, $data);
			}
		} else {
			foreach ($ids as $id) {
				$store->put($id, DataSource::parseData($id));
			}
		}
		return static::filterStore($store);
	}
}

<?php

namespace Salaun\ComplexUpsert\Tests\TestClasses;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * This class represent record already in the database that are required by the dataset to be upserted.
 */
class UpsertReference extends Model
{
	use HasFactory;

	protected $fillable = [
		'name',
		'description',
	];

	public function upsertModels()
	{
		return $this->hasMany(UpsertModel::class);
	}
}

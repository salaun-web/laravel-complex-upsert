<?php

namespace Salaun\ComplexUpsert\Tests\TestClasses;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Salaun\ComplexUpsert\Tests\Database\Factories\UpsertMorphicRelationFactory;
use Salaun\ComplexUpsert\Traits\HasModelValidation;
use Salaun\ComplexUpsert\Traits\HasOrderedJsonAttributes;
use Salaun\ComplexUpsert\Traits\HasUpserts;

class UpsertMorphicRelation extends Model
{
	use HasFactory;
	use HasOrderedJsonAttributes;
	use HasModelValidation;
	use HasUpserts;

	public const MORPH_NAME = 'upsert_morphic_relationable';

	protected static array $upsertId = [
		'upsert_morphic_relationable_type',
		'name',
	];

	protected $fillable = [
		'name',
		'description',
	];

	protected static function getRules(): array
	{
		if (static::$rules === null) return [
			'name' => ['required', 'max:15'],
			'description' => ['required', 'max:30'],
		];
		return static::$rules;
	}

	/**
	 * Get the parent parent model.
	 */
	public function upsertMorphicRelationable()
	{
		return $this->morphTo();
	}

	public function upsertModel()
	{
		return $this->belongsTo(UpsertModel::class);
	}
}

<?php

namespace Salaun\ComplexUpsert\Tests\TestClasses;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Salaun\ComplexUpsert\Traits\HasModelValidation;
use Salaun\ComplexUpsert\Traits\HasOrderedJsonAttributes;
use Salaun\ComplexUpsert\Traits\HasUpsertReferenceCollections;
use Salaun\ComplexUpsert\Traits\HasUpserts;

/**
 * This class represent record already in the database that are required by the dataset to be upserted.
 */
class UpsertPivot extends Pivot
{
	use HasOrderedJsonAttributes;
	use HasModelValidation;
	use HasUpserts;
	use HasUpsertReferenceCollections;
	use HasFactory;

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'upsert_pivots';

	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;

	protected static bool $canFail = true;

	protected static array $upsertId = [
		'upsert_model_id',
		'upsert_pivoted_id',
	];

	protected $fillable = [
		'active',
	];

	protected static function getRules(): array
	{
		if (static::$rules === null) return [
			'active' => ['boolean'],
		];
		return static::$rules;
	}

	public function upsertModel()
	{
		return $this->belongsTo(UpsertModel::class);
	}

	public function upsertPivoted()
	{
		return $this->belongsTo(UpsertPivoted::class);
	}

	public function registerUpsertReferenceCollections(): static
	{
		$this->addUpsertReferenceCollection(
			relation: 'upsertPivoted',
			referenceId: ['name'],
		);

		return $this;
	}
}

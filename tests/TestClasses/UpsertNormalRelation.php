<?php

namespace Salaun\ComplexUpsert\Tests\TestClasses;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Salaun\ComplexUpsert\Tests\Database\Factories\UpsertNormalRelationlFactory;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertModel;
use Salaun\ComplexUpsert\Traits\HasModelValidation;
use Salaun\ComplexUpsert\Traits\HasOrderedJsonAttributes;
use Salaun\ComplexUpsert\Traits\HasUpserts;

class UpsertNormalRelation extends Model
{
	use HasFactory;
	use HasOrderedJsonAttributes;
	use HasModelValidation;
	use HasUpserts;

	protected static array $upsertId = [
		'name',
	];

	protected $fillable = [
		'name',
		'description',
	];

	protected static function getRules(): array
	{
		if (static::$rules === null) return [
			'name' => ['required', 'max:15'],
			'description' => ['required', 'max:30'],
		];
		return static::$rules;
	}

	public function upsertModel()
	{
		return $this->belongsTo(UpsertModel::class);
	}
}

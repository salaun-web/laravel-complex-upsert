<?php

namespace Salaun\ComplexUpsert\Tests\TestClasses;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Salaun\ComplexUpsert\Models\UpsertedModel;
use Salaun\ComplexUpsert\Tests\Database\Factories\UpsertModelFactory;
use Salaun\ComplexUpsert\Traits\HasModelValidation;
use Salaun\ComplexUpsert\Traits\HasOrderedJsonAttributes;
use Salaun\ComplexUpsert\Traits\HasUpsertReferenceCollections;
use Salaun\ComplexUpsert\Traits\HasUpserts;

class UpsertModel extends Model
{
	use HasFactory;
	use HasOrderedJsonAttributes;
	use HasModelValidation;
	use HasUpserts;
	use HasUpsertReferenceCollections;

	protected array $upsertRelated = [];

	protected static array $upsertId = [
		'lang',
		'slug',
		'upsert_reference_id',
	];

	protected $fillable = [
		'lang',
		'name',
		'value',
		'optional',
	];

	protected static function getRules(): array
	{
		if (static::$rules === null) static::$rules = [
			'lang' => ['required', 'size:2'],
			'name' => ['required', 'max:15'],
			'value' => ['required', 'max:15'],
			'optional' => ['max:15'],
		];
		return static::$rules;
	}

	/**
	 * Fill the model with an array of attributes.
	 *
	 * @param  array  $attributes
	 * @return $this
	 *
	 * @throws \Illuminate\Database\Eloquent\MassAssignmentException
	 */
	public function setAttribute($key, $value)
	{
		parent::setAttribute($key, $value);
		if ($key === 'name') parent::setAttribute('slug', Str::slug($this->name));
		return $this;
	}

	public function upsertNormalRelations()
	{
		return $this->hasMany(UpsertNormalRelation::class);
	}

	public function upsertMorphicRelations()
	{
		return $this->morphMany(UpsertMorphicRelation::class, UpsertMorphicRelation::MORPH_NAME);
	}

	public function upsertReference()
	{
		return $this->belongsTo(UpsertReference::class);
	}

	public function upsertReferenceSync()
	{
		return $this->belongsTo(UpsertReference::class, 'upsert_reference_id');
	}

	public function upsertPivots()
	{
		return $this->hasMany(UpsertPivot::class);
	}

	public function upsertPivoted()
	{
		return $this->belongsToMany(UpsertPivoted::class, (new UpsertPivot())->getTable());
	}

	public function registerUpsertReferenceCollections(): static
	{
		$this->addUpsertReferenceCollection(
			relation: 'upsertReference',
			referenceId: ['name'],
		)->addUpsertReferenceCollection(
			relation: 'upsertReferenceSync',
			referenceId: ['name'],
			query: UpsertReference::query(),
			syncColumns: ['description'],
		);

		return $this;
	}

	public function latestUpsertedModel()
	{
		return $this->morphOne(UpsertedModel::class, UpsertedModel::MORPH_UPSERTABLE)->latestOfMany(['created_at', 'id']);
	}
}

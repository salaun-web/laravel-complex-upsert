<?php

namespace Salaun\ComplexUpsert\Tests\TestClasses;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * This class represent record already in the database that are required by the dataset to be upserted.
 */
class UpsertPivoted extends Model
{
	use HasFactory;

	protected $table = 'upsert_pivoted';

	protected static array $upsertId = [
		'name'
	];

	protected $fillable = [
		'name',
		'description',
	];

	public function upsertPivots()
	{
		return $this->hasMany(UpsertPivot::class)->using(UpsertPivot::class);
	}

	public function upsertModels()
	{
		return $this->belongsToMany(UpsertModel::class, (new UpsertPivot())->getTable())->using(UpsertPivot::class);
	}
}

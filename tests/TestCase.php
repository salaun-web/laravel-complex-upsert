<?php

namespace Salaun\ComplexUpsert\Tests;

use Illuminate\Support\Facades\Facade;
use Orchestra\Testbench\TestCase as TestbenchTestCase;
use Salaun\ComplexUpsert\ComplexUpsertServiceProvider;
use Tpetry\PostgresqlEnhanced\PostgresqlEnhancedServiceProvider;
use Illuminate\Database\Eloquent\Factories\Factory;

class TestCase extends TestbenchTestCase
{
	protected function getPackageProviders($app)
	{
		return [
			ComplexUpsertServiceProvider::class,
			PostgresqlEnhancedServiceProvider::class,
		];
	}

	public function getEnvironmentSetUp($app)
	{
		// config()->set('database.default', 'mysql');
		Facade::setFacadeApplication($app);
		Factory::guessFactoryNamesUsing(
			fn (string $modelName) => '\\Salaun\\ComplexUpsert\\Tests\\Database\\Factories\\' . class_basename($modelName) . 'Factory'
		);
	}

	/**
	 * Define database migrations.
	 *
	 * @return void
	 */
	protected function defineDatabaseMigrations()
	{
		$this->loadMigrationsFrom(__DIR__ . '/Database/Migrations/');
	}
}

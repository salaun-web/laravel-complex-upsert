<?php

use Salaun\ComplexUpsert\Classes\UpsertReferenceService;
use Salaun\ComplexUpsert\Classes\UpsertService;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertModel;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertMorphicRelation;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertNormalRelation;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertPivot;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertReference;

beforeEach(function () {
	UpsertService::cleanUp(true);
	UpsertReferenceService::cleanUp(true);
});

test('can create records', function () {
	$parent = UpsertModel::factory()->make();
	$parents = [$parent];

	UpsertService::massUpsert($parents);

	$this->assertDatabaseHas($parent->getTable(), $parent->getAttributes());
});


test('can update records', function () {
	$parent = UpsertModel::factory()->create();
	$this->assertDatabaseHas($parent->getTable(), $parent->getAttributes());

	$parent->optional = 'opt';
	$parents = [$parent];
	UpsertService::massUpsert($parents);
	$this->assertDatabaseHas($parent->getTable(), $parent->getAttributes());
});

test('can make all CRUD operations', function (array $names, array $models, array $relations) {
	$rels = [];
	foreach ($names as $key => $name) {
		$parents[$name] = $models[$key];
		$rels[$name] = [];
		foreach ($relations as $relationName => $relation) {
			$rels[$name][$relationName] = $relation()->count(3)->make();
			$parents[$name]->setReverseRelation($rels[$name][$relationName], ...explode(':', $relationName));
		}
	}

	$relsOriginal = serialize($rels);
	$parentsOriginal = serialize($parents);

	UpsertService::massUpsert($parents);
	UpsertService::get()->pruneType();
	UpsertService::cleanUp();

	$rels = unserialize($relsOriginal);
	$parents = unserialize($parentsOriginal);
	foreach ($rels['static'] as $relationName => $relation) {
		$parents['static']->setReverseRelation($relation, ...explode(':', $relationName));
	}

	$parents['updated']->value += 99;
	foreach ($relations as $relationName => $relation) {
		$parents['updated']->setReverseRelation($relation()->count(3)->make(), ...explode(':', $relationName));
	}

	foreach ($rels['prunned'] as $relationName => $relation) {
		$parents['prunned']->setReverseRelation(collect([$rels['prunned'][$relationName]->first()]), ...explode(':', $relationName));
	}

	UpsertService::massUpsert($parents);
	UpsertService::get()->pruneType();
	UpsertService::cleanUp();

	$this->assertEquals(
		count($parents),
		UpsertModel::whereInMulti(
			reset($models)->getUpsertId(),
			collect($parents)->map(fn ($model) => $model->getAttributes())->all()
		)->count()
	);
	expect(collect($parents)->first()->getRelations())
		->toHaveKeys(['upsertNormalRelations', 'upsertMorphicRelations', 'upsertPivots']);
	$this->assertDatabaseCount('upsert_normal_relations', 7);
	$this->assertDatabaseCount('upsert_morphic_relations', 7);
	$this->assertDatabaseCount('upsert_pivots', 7);
	$this->assertDatabaseCount('upsert_pivoted', 12);
	$this->assertDatabaseCount('upsert_references', 1);
})->with([
	'default' => [
		'names' => ['static', 'updated', 'prunned'],
		'models' => fn () => UpsertModel::factory()->count(3)->for(UpsertReference::factory(), 'upsertReference')->make()->all(),
		'relations' => [
			'upsertNormalRelations:upsertModel' => fn () => UpsertNormalRelation::factory(),
			'upsertMorphicRelations:upsertMorphicRelationable' => fn () => UpsertMorphicRelation::factory(),
			'upsertPivots:upsertModel' => fn () => UpsertPivot::factory(),
		]
	]
]);

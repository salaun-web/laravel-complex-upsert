<?php

use Salaun\ComplexUpsert\Classes\UpsertReferenceService;
use Salaun\ComplexUpsert\Classes\UpsertService;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertModel;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertMorphicRelation;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertNormalRelation;

beforeEach(function () {
	UpsertService::cleanUp(true);
	UpsertReferenceService::cleanUp(true);
});

test('can upsert normal relations', function () {
	$parent = UpsertModel::factory()->create();
	$child0 = UpsertNormalRelation::factory()->make();
	$child0->upsertModel()->associate($parent);
	$child0->save();

	$parent->optional = 'opt';
	$child1 = UpsertNormalRelation::factory()->make();
	$parent->addReverseRelation($child1, 'upsertNormalRelations', 'upsertModel');
	$parents = [$parent];
	UpsertService::massUpsert($parents);

	$this->assertModelExists($parent);
	$this->assertModelExists($child0);
	$this->assertModelExists($child1);
});

test('can upsert morphic relations', function () {
	$parent = UpsertModel::factory()->create();
	$child0 = UpsertMorphicRelation::factory()->make();
	$child0->upsertMorphicRelationable()->associate($parent);
	$child0->save();

	$parent->optional = 'opt';
	$child1 = UpsertMorphicRelation::factory()->make();
	$parent->addReverseRelation($child1, 'upsertMorphicRelations', 'upsertMorphicRelationable');
	$parents = [$parent];
	UpsertService::massUpsert($parents);

	$this->assertModelExists($parent);
	$this->assertModelExists($child0);
	$this->assertModelExists($child1);
});

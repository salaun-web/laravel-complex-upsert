<?php

use Salaun\ComplexUpsert\Classes\UpsertReferenceService;
use Salaun\ComplexUpsert\Classes\UpsertService;
use Salaun\ComplexUpsert\Models\UpsertedModel;
use Salaun\ComplexUpsert\Models\UpsertProcess;
use Salaun\ComplexUpsert\Models\UpsertProcessDispatcher;
use Salaun\ComplexUpsert\Tests\Bulk\BulkProcessor;
use Salaun\ComplexUpsert\Tests\Bulk\BulkProcessorDispatcher;
use Salaun\ComplexUpsert\Tests\Bulk\DataSource;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertModel;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertNormalRelation;

beforeEach(function () {
	UpsertService::cleanUp(true);
	UpsertReferenceService::cleanUp(true);
});

test('can run processor', function () {

	$this->assertDatabaseCount((new UpsertModel())->getTable(), 0);

	$process = new UpsertProcess();
	$processor = new BulkProcessor($process);
	$processor::dispatchSync($process);

	$this->assertDatabaseCount((new UpsertProcessDispatcher())->getTable(), 0);
	$this->assertDatabaseCount((new UpsertProcess())->getTable(), 1);
	$this->assertDatabaseCount((new UpsertedModel())->getTable(), 2);
	$this->assertDatabaseCount((new UpsertModel())->getTable(), 2);
	$this->assertDatabaseCount((new UpsertNormalRelation())->getTable(), 4);
	$this->assertCount(8, UpsertProcess::sole()->statistics['time']);
	$this->assertCount(4, UpsertProcess::sole()->statistics);
});

test('can dispatch processors', function () {

	$this->assertDatabaseCount((new UpsertProcessDispatcher())->getTable(), 0);

	$processDispatcher = new UpsertProcessDispatcher(['ids' => DataSource::getIds()]);
	BulkProcessorDispatcher::dispatchBus($processDispatcher);

	$this->assertDatabaseCount((new UpsertProcessDispatcher())->getTable(), 1);
	$this->assertDatabaseCount((new UpsertProcess())->getTable(), 2);
	$this->assertDatabaseCount((new UpsertedModel())->getTable(), 2);
	$this->assertDatabaseCount((new UpsertModel())->getTable(), 2);
	$this->assertDatabaseCount((new UpsertNormalRelation())->getTable(), 4);
	$this->assertCount(8, UpsertProcess::first()->statistics['time']);
	$this->assertCount(4, UpsertProcess::first()->statistics);
});

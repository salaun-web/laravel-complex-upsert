<?php

use Salaun\ComplexUpsert\Classes\UpsertReferenceService;
use Salaun\ComplexUpsert\Classes\UpsertService;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertModel;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertPivot;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertPivoted;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertReference;

beforeEach(function () {
	UpsertService::cleanUp(true);
	UpsertReferenceService::cleanUp(true);
});

test('can fetch references', function () {
	// Setup
	$reference = new UpsertReference([
		'name' => 'a name',
		'description' => 'a description',
	]);

	$upsertReference = new UpsertReference([
		'name' => 'a name',
		'description' => 'another description',
	]);

	$parent = UpsertModel::factory()->makeOne();
	$parent->setUpsertReference('upsertReference', $upsertReference->getAttributes());

	// Inserting
	$reference->save();
	$this->assertCount(1, $reference::get());
	$this->assertDatabaseHas($reference->getTable(), $reference->getAttributes());

	$parents = [$parent];
	UpsertReferenceService::resolveUpsertRefences($parents);
	$this->assertCount(1, $reference::get());

	UpsertService::massUpsert($parents);
	$this->assertDatabaseHas($parent->getTable(), $parent->getAttributes());
	$this->assertEquals($reference->getKey(), $parent->upsert_reference_id);
	$this->assertDatabaseHas($reference->getTable(), $reference->getAttributes());
});

test('can sync references', function () {
	// Setup
	$savedReference = new UpsertReference([
		'name' => 'a name',
		'description' => 'a description',
	]);

	$upsertReference00 = new UpsertReference([
		'name' => 'a name',
		'description' => 'another description',
	]);

	$upsertReference01 = new UpsertReference([
		'name' => 'and another',
		'description' => 'and another one',
	]);

	$parent00 = UpsertModel::factory()->makeOne();
	$parent00->setUpsertReference('upsertReferenceSync', $upsertReference00->getAttributes());

	$parent01 = UpsertModel::factory()->makeOne();
	$parent01->setUpsertReference('upsertReferenceSync', $upsertReference01->getAttributes());

	// Inserting
	$savedReference->save();
	$this->assertCount(1, $savedReference::get());
	$this->assertDatabaseHas($savedReference->getTable(), $savedReference->getAttributes());


	$parents = [$parent00, $parent01];
	UpsertReferenceService::resolveUpsertRefences($parents);
	$savedReferences = $savedReference::get();
	$this->assertCount(2, $savedReferences);

	UpsertService::massUpsert($parents);
	$this->assertDatabaseHas($parent00->getTable(), $parent00->getAttributes());

	$this->assertEquals($savedReference->getKey(), $parent00->upsert_reference_id);
	$this->assertDatabaseHas($savedReference->getTable(), $upsertReference00->getAttributes());

	expect($parent01->upsert_reference_id)->not()->toBe($savedReference->getKey());
	$this->assertDatabaseHas($savedReference->getTable(), $upsertReference01->getAttributes());
});

test('can upsert pivot references', function () {
	// // Setup
	// Create references instances
	$pivoted0 = new UpsertPivoted([
		'name' => 'a name 0',
		'description' => 'a description of zero',
	]);
	$pivoted1 = new UpsertPivoted([
		'name' => 'a name 1',
		'description' => 'a description of one',
	]);

	// Create a parent
	$parent = UpsertModel::factory()->makeOne();

	// Setup pivots on parent
	$pivot0 = UpsertPivot::makeValid(['active' => true]);
	$pivot0->setUpsertReference('upsertPivoted', ['name' => 'a name 0']);

	$pivot1 = UpsertPivot::makeValid(['active' => false]);
	$pivot1->setUpsertReference('upsertPivoted', ['name' => 'a name 1']);

	$pivot2 = UpsertPivot::makeValid(['active' => true]);
	$pivot2->setUpsertReference('upsertPivoted', ['name' => 'a name 2']);

	$parent->addReverseRelation($pivot0);
	$parent->addReverseRelation($pivot1);
	$parent->addReverseRelation($pivot2);

	// // Inserting
	$pivoted0->save();
	$this->assertDatabaseHas($pivoted0->getTable(), $pivoted0->getAttributes());
	$pivoted1->save();
	$this->assertDatabaseHas($pivoted1->getTable(), $pivoted1->getAttributes());

	$parents = [$parent];
	UpsertReferenceService::resolveUpsertRefences($parents);
	UpsertService::massUpsert($parents);

	// // Asserting
	$this->assertDatabaseHas($parent->getTable(), $parent->getAttributes());

	$parent->load('upsertPivoted')->get();
	$this->assertDatabaseHas($pivot0->getTable(), $pivot0->getAttributes());
	$this->assertDatabaseHas($pivot1->getTable(), $pivot1->getAttributes());
	$this->assertDatabaseHas($pivot2->getTable(), $pivot2->getAttributes());
});

test('can upsert pivot references with can fail', function () {
	// // Setup
	// Create references instances
	$pivoted0 = new UpsertPivoted([
		'name' => 'a name 0',
		'description' => 'a description of zero',
	]);

	// Create a parent
	$parent = UpsertModel::factory()->makeOne();

	// Setup pivots on parent
	$pivot0 = UpsertPivot::makeValid(['active' => true]);
	$pivot0->setUpsertReference('upsertPivoted', ['name' => 'a name 0']);

	$pivot1 = UpsertPivot::makeValid(['active' => true]);
	$pivot1->setUpsertReference('upsertPivoted', ['name' => 'a name 1']);

	$parent->addReverseRelation($pivot0);
	$parent->addReverseRelation($pivot1);

	// // Inserting
	$pivoted0->save();
	$this->assertDatabaseHas($pivoted0->getTable(), $pivoted0->getAttributes());

	$parents = [$parent];
	UpsertReferenceService::resolveUpsertRefences($parents);
	UpsertService::massUpsert($parents);

	// // Asserting
	$this->assertDatabaseHas($parent->getTable(), $parent->getAttributes());

	$parent->load('upsertPivoted')->get();
	$this->assertCount(1, $parent->getRelation('upsertPivoted'));
	$this->assertDatabaseHas($pivot0->getTable(), $pivot0->getAttributes());
});

<?php

use Illuminate\Support\Facades\DB;
use Salaun\ComplexUpsert\Classes\UpsertService;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertModel;

test('can query where in multi with one column', function () {
	$columns = [
		'lang',
	];

	$values = [
		'lang' => 'FR',
		'name' => 'my product',
		'slug' => 'my_product',
		'value' => '9999',
	];

	DB::table('upsert_models')->insert($values);
	$this->assertDatabaseHas('upsert_models', $values);

	$record = UpsertModel::whereInMulti($columns, [$values])->get();
	$this->assertCount(1, $record);
	$this->assertEquals($values, array_intersect_assoc($values, $record->first()->getAttributes()));
});

test(
	'can query where in multi with multiple columns',
	function () {
		$reference = [
			'name' => 'a name',
			'description' => 'a description',
		];

		$columns = [
			'lang',
			'slug',
			'upsert_reference_id',
		];

		$values = [
			'lang' => 'FR',
			'name' => 'my product',
			'slug' => 'my_product',
			'value' => '9999',
		];

		$refId = DB::table('upsert_references')->insertGetId($reference);
		$this->assertDatabaseHas('upsert_references', $reference);

		$values['upsert_reference_id'] = $refId;
		DB::table('upsert_models')->insert($values);
		$this->assertDatabaseHas('upsert_models', $values);

		$record = UpsertModel::query()->whereInMulti($columns, [$values])->get();
		$this->assertCount(1, $record);
		$this->assertEquals($values, array_intersect_assoc($values, $record->first()->getAttributes()));
	}
);

test('can query where in multi with a nullable column',	function () {
	$columns = [
		'lang',
		'slug',
		'upsert_reference_id',
	];

	$values = [
		'lang' => 'FR',
		'name' => 'my product',
		'slug' => 'my_product',
		'value' => '9999',
	];

	DB::table('upsert_models')->insert($values);
	$this->assertDatabaseHas('upsert_models', $values);

	$record = UpsertModel::query()->whereInMulti($columns, [$values])->get();
	$this->assertCount(1, $record);
	$this->assertEquals($values, array_intersect_assoc($values, $record->first()->getAttributes()));
});

test('can query where in multi with multiple records',	function () {
	$reference = [
		'name' => 'a name',
		'description' => 'a description',
	];

	$columns = [
		'lang',
		'slug',
		'upsert_reference_id',
	];

	$values = [
		0 => [
			'lang' => 'FR',
			'name' => 'my product 0',
			'slug' => 'my_product_0',
			'value' => '99990',
			'upsert_reference_id' => null,
		],
		1 => [
			'lang' => 'FR',
			'name' => 'my product 1',
			'slug' => 'my_product_1',
			'value' => '99991',
			'upsert_reference_id' => null,
		],
		2 => [
			'lang' => 'FR',
			'name' => 'my product 2',
			'slug' => 'my_product_2',
			'value' => '99992',
			'upsert_reference_id' => null,
		]
	];

	$refId = DB::table('upsert_references')->insertGetId($reference);
	$this->assertDatabaseHas('upsert_references', $reference);

	$values[0]['upsert_reference_id'] = $refId;

	DB::table('upsert_models')->insert($values);
	$this->assertDatabaseHas('upsert_models', $values[0]);
	$this->assertDatabaseHas('upsert_models', $values[1]);
	$this->assertDatabaseHas('upsert_models', $values[2]);

	$record = UpsertModel::query()->whereInMulti($columns, [$values[0], $values[1]])->get();
	$this->assertCount(2, $record);
	$this->assertEquals($values[0], array_intersect_assoc($values[0], $record[0]->getAttributes()));
	$this->assertEquals($values[1], array_intersect_assoc($values[1], $record[1]->getAttributes()));
});

test('can query where in multi with an empty array',	function () {
	$columns = [];

	$values = [
		'lang' => 'FR',
		'name' => 'my product',
		'slug' => 'my_product',
		'value' => '9999',
	];

	DB::table('upsert_models')->insert($values);
	$this->assertDatabaseHas('upsert_models', $values);

	$record = UpsertModel::query()->whereInMulti($columns, [$values])->get();
	$this->assertCount(0, $record);
});

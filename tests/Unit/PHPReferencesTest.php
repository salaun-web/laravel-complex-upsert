<?php

/**
 * A variable will contain a value and this value lives in RAM at a certain adresse.
 * Multiple variable with the same reference will all point to the same RAM adress
 * It is like a capsule containg a value, between variable (accessible by the programme)
 * and the data (set in the system RAM).
 *
 * References are a bit treaky to deal with but can be a life saver during heavy processes.
 * It reduces ram usage by stocking the value only once per container (variable, array key, etc...).
 * It also allows you to apply the object change everywhere in the process.
 * In that way i like to compare it with events.
 *
 * What you need to keep in mind is that PHP GC (Garbage Collector) will do it's best to remove every unsused refernces.
 * PHP will also try not to break the reference if it can get away with it.
 * So unless the container is changed in any way (ex. array_chunk) the reference will be kept.
 *
 * @link https://www.php.net/manual/en/language.references.php
 * @link https://www.php.net/manual/fr/control-structures.foreach.php
 */

use Illuminate\Support\Collection;

test('can keep references in variables', function () {
	$variable = 'var';
	$reference = &$variable;

	$reference = 'ref';

	expect($variable)->toEqual('ref');
});

test('can keep references of arrays', function () {
	$variables = ['var1', 'var2', 'var3', null];
	$references = &$variables;

	$references = array_filter($references);

	expect($variables)->toHaveCount(3);
});

test('can keep references in arrays', function () {
	$variables = [];
	$references = [];

	foreach (['var1', 'var2', 'var3'] as $key => &$var) {
		$variables[$key] = &$var;
		$references[$key] = &$var;
	}
	unset($var);

	$references[0] = null;

	expect($variables[0])->toBeNull();
	expect(array_filter($variables))->toHaveCount(2);
});

test('cannot keep references in arrays if items not declared as such', function () {
	$variables = ['var1', 'var2', 'var3'];
	$references = [];

	foreach ($variables as $key => $var) {
		$references[$key] = &$var;
	}
	unset($var);

	$references[0] = null;

	expect($variables[0])->not()->toBeNull();
});

test('cannot keep references in arrays if items not declared as such even directly', function () {
	$variables = ['var1', 'var2', 'var3'];
	$references = [];

	foreach ($variables as $key => &$var) {
		$references[$key] = $var;
	}
	unset($var);

	$references[0] = null;

	expect($variables[0])->not()->toBeNull();
});

test('can array_chunk arrays of references', function () {
	$variables = ['var1', 'var2', 'var3'];
	$references = [];
	$chunks = [];

	// array_chunk requires you to specifically tell him that values are references
	foreach ($variables as $key => &$var) {
		$variables[$key] = &$var;
		$references[$key] = &$var;
	}
	unset($var);

	$chunks = array_chunk($references, 10, true);
	$chunks[0][0] = null;

	expect($variables[0])->toBeNull();
	expect(array_filter($variables))->toHaveCount(2);
});

test('can break references by new assignation', function () {
	$variables = ['var1', 'var2', 'var3'];
	$references = [];
	$noReferences = [];

	foreach ($variables as $key => &$var) {
		$variables[$key] = $var;
		$references[$key] = $var;
	}
	unset($var);

	foreach ($variables as $key => $var) {
		$noReferences[$key] = $var;
	}

	$noReferences[0] = null;

	expect($variables[0])->not()->toBeNull();
});

test('can break references in array by unset', function () {
	$variables = ['var1', 'var2', 'var3'];
	$references = [];

	foreach ($variables as $key => &$var) {
		$variables[$key] = &$var;
		$references[$key] = &$var;
	}
	unset($var);

	foreach ($variables as $key => $var) {
		unset($references[$key]);
		$references[$key] = $var;
	}

	$noReferences[0] = null;

	expect($variables[0])->not()->toBeNull();
});

test('can test', function () {
	$variables = ['var1', 'var2', 'var3'];
	$references = [];
	$chunks = [];

	// array_chunk requires you to specifically tell him that values are references
	foreach ($variables as $key => &$var) {
		$variables[$key] = &$var;
		$references[$key] = &$var;
	}
	unset($var);

	$references = collect($references);
	$variables = collect($variables);

	externalFunction1($references);

	expect($variables[0])->toBeNull();
	expect($variables->filter())->toHaveCount(2);
});

function externalFunction1(Collection &$references)
{
	$ref = $references->all();
	externalFunction2($ref);
	$references = collect($ref);
}

function externalFunction2(array &$references)
{
	$chunks = array_chunk($references, 10, true);
	$chunks[0][0] = null;
}

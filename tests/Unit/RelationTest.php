<?php

use Salaun\ComplexUpsert\Tests\TestClasses\UpsertModel;
use Salaun\ComplexUpsert\Tests\TestClasses\UpsertNormalRelation;

test('can set reverse relation', function (?string $relationKey) {
	$relation = UpsertNormalRelation::factory()->make();
	$model = UpsertModel::factory()->make();

	$validModel = UpsertModel::makeValid(
		attributes: $model->getAttributes(),
		relations: [
			$relationKey => collect([$relation]),
		],
		references: [],
	);
	expect(array_keys($validModel->getRelations()))->toBe(['upsertNormalRelations::upsertModel']);
})->with([
	'anonymous reverse relation' => ['upsertNormalRelations'],
	'custom reverse relation' => ['upsertNormalRelations:upsertModel'],
]);

test('can make valid set reverse relation', function (?string $relationName = null, ?string $reverseRelationName = null) {
	$relation = UpsertNormalRelation::factory()->make();
	$model = UpsertModel::factory()->make();

	$model->setReverseRelation(collect([$relation]), $relationName, $reverseRelationName);
	expect(array_keys($model->getRelations()))->toBe(['upsertNormalRelations::upsertModel']);
})->with([
	'full auto' => [],
	'anonymous reverse relation' => ['upsertNormalRelations'],
	'custom reverse relation' => ['upsertNormalRelations', 'upsertModel'],
]);

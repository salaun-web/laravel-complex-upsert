<?php

use Salaun\ComplexUpsert\Tests\TestClasses\UpsertModel;

test('can generate upsert key', function () {
	$model = new UpsertModel([
		'lang' => 'FR',
		'name' => 'my product',
	]);

	$expected = '["FR","my-product",null]';

	$this->assertEquals($expected, $model->upsert_key);
	$this->assertEquals($expected, $model->getUpsertKeyAttribute());
	$this->assertEquals($expected, $model->getUpsertKeyAttribute($model->getAttributes()));
});

<?php
// config for Salaun/ComplexUpsert
return [
	'chunk_size' =>  env('COMPLEX_UPSERT_CHUNK_SIZE', 300),
];
